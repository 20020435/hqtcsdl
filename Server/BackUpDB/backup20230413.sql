-- MySQL dump 10.13  Distrib 8.0.32, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: production_move
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agent_ware_house`
--

DROP TABLE IF EXISTS `agent_ware_house`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `agent_ware_house` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `modifiedby` varchar(255) DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `distribution_id` bigint DEFAULT NULL,
  `warehouse_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDXlelp9v8fog620ih0tkreu5l11` (`distribution_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agent_ware_house`
--

LOCK TABLES `agent_ware_house` WRITE;
/*!40000 ALTER TABLE `agent_ware_house` DISABLE KEYS */;
/*!40000 ALTER TABLE `agent_ware_house` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `branch`
--

DROP TABLE IF EXISTS `branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `branch` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branch`
--

LOCK TABLES `branch` WRITE;
/*!40000 ALTER TABLE `branch` DISABLE KEYS */;
/*!40000 ALTER TABLE `branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `modifiedby` varchar(255) DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `content` text,
  `cylinder_capacity` varchar(255) DEFAULT NULL,
  `engine_type` varchar(255) DEFAULT NULL,
  `fuel_pump_system` varchar(255) DEFAULT NULL,
  `gear` varchar(255) DEFAULT NULL,
  `image` text,
  `max_speed` varchar(255) DEFAULT NULL,
  `maximum_output` varchar(255) DEFAULT NULL,
  `maximum_torque` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` bigint DEFAULT NULL,
  `short_description` text,
  `tank_capacity` varchar(255) DEFAULT NULL,
  `warranty_period` bigint DEFAULT '24',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distribution_agent`
--

DROP TABLE IF EXISTS `distribution_agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `distribution_agent` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `modifiedby` varchar(255) DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `branch_id` bigint DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX50jaws2j8qky7f5fv4kxhlodd` (`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distribution_agent`
--

LOCK TABLES `distribution_agent` WRITE;
/*!40000 ALTER TABLE `distribution_agent` DISABLE KEYS */;
/*!40000 ALTER TABLE `distribution_agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distribution_history`
--

DROP TABLE IF EXISTS `distribution_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `distribution_history` (
  `createddate` date NOT NULL,
  `id` bigint NOT NULL,
  `createdby` varchar(255) DEFAULT NULL,
  `modifiedby` varchar(255) DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `category_id` bigint DEFAULT NULL,
  `count` bigint DEFAULT NULL,
  `distribution_agent_id` bigint DEFAULT NULL,
  `factory_id` bigint DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `product_code_list` varchar(255) DEFAULT NULL,
  `status` bigint DEFAULT NULL,
  PRIMARY KEY (`createddate`,`id`),
  KEY `IDX8152mjcumgr2mk3ajedo9ysf6` (`distribution_agent_id`),
  KEY `IDXleje9icpf0a56ubpncm0onqag` (`factory_id`),
  KEY `IDX9rlha0cmh4qh82vuf4xgu5ib8` (`distribution_agent_id`,`status`),
  KEY `IDXsmr1vf9f18j7bo4dij8l3ldh4` (`factory_id`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distribution_history`
--

LOCK TABLES `distribution_history` WRITE;
/*!40000 ALTER TABLE `distribution_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `distribution_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factory`
--

DROP TABLE IF EXISTS `factory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `factory` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `modifiedby` varchar(255) DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `branch_id` bigint DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `ware_house_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factory`
--

LOCK TABLES `factory` WRITE;
/*!40000 ALTER TABLE `factory` DISABLE KEYS */;
/*!40000 ALTER TABLE `factory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (1);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `createddate` date NOT NULL,
  `id` bigint NOT NULL,
  `createdby` varchar(255) DEFAULT NULL,
  `modifiedby` varchar(255) DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `code` varchar(255) NOT NULL,
  `distribution_agent_id` bigint DEFAULT NULL,
  `factory_id` bigint DEFAULT NULL,
  `status` int DEFAULT NULL,
  `service_center_id` bigint DEFAULT NULL,
  `warranty_count` bigint DEFAULT '0',
  `warranty_period_date` datetime DEFAULT NULL,
  `category_id` bigint DEFAULT NULL,
  `ware_house_id` bigint DEFAULT NULL,
  PRIMARY KEY (`createddate`,`id`),
  UNIQUE KEY `UK_h3w5r1mx6d0e5c6um32dgyjej` (`code`),
  KEY `IDXh3w5r1mx6d0e5c6um32dgyjej` (`code`),
  KEY `IDXi0vry5cr5df8il8d0cxh39lnm` (`status`),
  KEY `IDXrlaghtegr0yx2c1q1s6nkqjlh` (`category_id`),
  KEY `IDXl17vef6ish69u0bskua5b66e8` (`ware_house_id`),
  KEY `IDX7xs56rpvvgm6jbw3yei14v6c9` (`factory_id`,`status`),
  KEY `IDX7f23m6lb7aqn4cn8aygrf57bp` (`category_id`,`status`),
  KEY `IDXti7l9nynjxfvmjc8hq88vkx18` (`ware_house_id`,`category_id`,`status`),
  KEY `IDXtl6psadtd1mg1mym0lx6a9922` (`distribution_agent_id`,`category_id`,`status`),
  CONSTRAINT `FK10n3sc4rce95qvlwsnfa4kbe6` FOREIGN KEY (`ware_house_id`) REFERENCES `ware_house` (`id`),
  CONSTRAINT `FK1mtsbur82frn64de7balymq9s` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `production_history`
--

DROP TABLE IF EXISTS `production_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `production_history` (
  `createddate` date NOT NULL,
  `id` bigint NOT NULL,
  `createdby` varchar(255) DEFAULT NULL,
  `modifiedby` varchar(255) DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `category_id` bigint DEFAULT NULL,
  `count` bigint DEFAULT NULL,
  `factory_id` bigint DEFAULT NULL,
  PRIMARY KEY (`createddate`,`id`),
  KEY `IDXtl1tb3w828cgic4tnt43jt2f3` (`factory_id`),
  KEY `IDXth5esvv99gjovrp0w319talh0` (`category_id`),
  KEY `IDXff2tehbbwl5dst3ykvjepwwkj` (`factory_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
/*!50100 PARTITION BY RANGE (to_days(`createddate`))
(PARTITION p20230412 VALUES LESS THAN (738987) ENGINE = InnoDB,
 PARTITION p20230413 VALUES LESS THAN (738988) ENGINE = InnoDB) */;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `production_history`
--

LOCK TABLES `production_history` WRITE;
/*!40000 ALTER TABLE `production_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `production_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `refreshtoken`
--

DROP TABLE IF EXISTS `refreshtoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `refreshtoken` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `modifiedby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `token` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_or156wbneyk8noo4jstv55ii3` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `refreshtoken`
--

LOCK TABLES `refreshtoken` WRITE;
/*!40000 ALTER TABLE `refreshtoken` DISABLE KEYS */;
INSERT INTO `refreshtoken` VALUES (1,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `refreshtoken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ADMIN','ADMIN'),(2,'DISTRIBUTION_AGENT','DISTRIBUTION AGENT'),(3,'FACTORY','FACTORY'),(4,'SERVICE_CENTER','SERVICE CENTER');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_center`
--

DROP TABLE IF EXISTS `service_center`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service_center` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `modifiedby` varchar(255) DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `branch_id` bigint DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_center`
--

LOCK TABLES `service_center` WRITE;
/*!40000 ALTER TABLE `service_center` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_center` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_history`
--

DROP TABLE IF EXISTS `transaction_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction_history` (
  `createddate` date NOT NULL,
  `id` bigint NOT NULL,
  `createdby` varchar(255) DEFAULT NULL,
  `modifiedby` varchar(255) DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `customer_age` bigint DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `distribution_agent_id` bigint DEFAULT NULL,
  `product_id` bigint DEFAULT NULL,
  PRIMARY KEY (`createddate`,`id`),
  KEY `IDXmbw5d0g80rngta7ooss5f6e7m` (`product_id`),
  KEY `IDXd5cxm0abwye8yvalvmfjwmolk` (`distribution_agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_history`
--

LOCK TABLES `transaction_history` WRITE;
/*!40000 ALTER TABLE `transaction_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `modifiedby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL,
  `address` text CHARACTER SET utf8mb3 COLLATE utf8mb3_bin,
  `fullname` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `image` text CHARACTER SET utf8mb3 COLLATE utf8mb3_bin,
  `password` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `phonenumber` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `refresh_token_id` bigint NOT NULL,
  `role_id` bigint DEFAULT NULL,
  `branch_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKo3wsa4hvodpbgd6upolukg7nj` (`refresh_token_id`),
  KEY `FKn82ha3ccdebhokx3a8fgdqeyy` (`role_id`),
  KEY `FK9yy0ya980j002yvtxi9r7kv6b` (`branch_id`),
  CONSTRAINT `FK9yy0ya980j002yvtxi9r7kv6b` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`),
  CONSTRAINT `FKn82ha3ccdebhokx3a8fgdqeyy` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FKo3wsa4hvodpbgd6upolukg7nj` FOREIGN KEY (`refresh_token_id`) REFERENCES `refreshtoken` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,NULL,NULL,NULL,NULL,NULL,'ADMIN',NULL,'$2a$10$fXPNGnniIlAtoBpGQGZBbuCScdj4Pdnj.BLd2lujHr0cYo2z7epzy',NULL,'admin',1,1,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ware_house`
--

DROP TABLE IF EXISTS `ware_house`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ware_house` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `modifiedby` varchar(255) DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ware_house`
--

LOCK TABLES `ware_house` WRITE;
/*!40000 ALTER TABLE `ware_house` DISABLE KEYS */;
/*!40000 ALTER TABLE `ware_house` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warranty_history`
--

DROP TABLE IF EXISTS `warranty_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `warranty_history` (
  `createddate` date NOT NULL,
  `id` bigint NOT NULL,
  `createdby` varchar(255) DEFAULT NULL,
  `modifiedby` varchar(255) DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `distribution_agent_id` bigint DEFAULT NULL,
  `error_message` varchar(255) DEFAULT NULL,
  `factory_id` bigint DEFAULT NULL,
  `product_id` bigint DEFAULT NULL,
  `service_center_id` bigint DEFAULT NULL,
  `status` bigint DEFAULT NULL,
  PRIMARY KEY (`createddate`,`id`),
  KEY `IDXocbdms7u5r4fbch5wwgc4jfjp` (`distribution_agent_id`),
  KEY `IDXoqg73mm3ig1ykwaiwsqv930gd` (`distribution_agent_id`,`status`),
  KEY `IDXr6g7166oxtyeqwbjn6s1tyva2` (`service_center_id`),
  KEY `IDX3g8mh7sarqfwuesyr79kt9u2x` (`service_center_id`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warranty_history`
--

LOCK TABLES `warranty_history` WRITE;
/*!40000 ALTER TABLE `warranty_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `warranty_history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-04-13 10:27:54
