package com.ptweb.production.util;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;

public class DateUtils
{
    public static String getDateByFormat(String format, Date date)
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }
}
