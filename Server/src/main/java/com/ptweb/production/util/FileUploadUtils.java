package com.ptweb.production.util;

import com.ptweb.production.service.impl.GoogleDiverService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */
public class FileUploadUtils
{
	public static String uploadFileToGG(GoogleDiverService googleDiverService, MultipartFile multipartFile) throws IOException
	{
		String fileCode = RandomStringUtils.randomAlphanumeric(8);
		try (InputStream inputStream = multipartFile.getInputStream())
		{
			String name = "Files-Upload/" + fileCode + "-" + multipartFile.getName();
			File file = new File(name);
			file.getParentFile().mkdirs();
			FileOutputStream outputStream = new FileOutputStream(file);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = inputStream.read(buffer)) > 0)
			{
				outputStream.write(buffer, 0, length);
			}
			String url = googleDiverService.addGoogleDriveFiles(file, name);
			outputStream.close();
			file.delete();
			return url;
		}
		catch (IOException e)
		{
			throw new IOException("Could not save file:" + multipartFile.getName(), e);
		}
	}
}
