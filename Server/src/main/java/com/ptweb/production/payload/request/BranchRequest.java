package com.ptweb.production.payload.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BranchRequest
{
	private Long id;
	private String name;
	private String address;
	private String role;
}
