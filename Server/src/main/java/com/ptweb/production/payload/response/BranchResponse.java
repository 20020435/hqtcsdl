package com.ptweb.production.payload.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BranchResponse
{
	private Long id;
	private String name;
	private String address;
	private String role;
}
