package com.ptweb.production.payload.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WarehouseResponse
{
	private Long id;
	private String name;
	private String address;
}
