package com.ptweb.production.payload.response;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class UserResponse
{
	private Long id;
	private String username;
	private String fullName;
	private String phoneNumber;
	private String image;
	private String address;
	private String role;
}
