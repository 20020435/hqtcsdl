package com.ptweb.production.payload.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentResponse
{
	private String productCode;
}
