package com.ptweb.production.payload.response;

import com.ptweb.production.entity.Category;
import com.ptweb.production.util.MapperUtils;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class DistributionHistoryResponse
{
	private String distributionAgent;
	private CategoryResponse category;
	private long count;
	private Date created;

	public DistributionHistoryResponse(String distributionAgent, Category category,
									   long count, Date created)
	{
		this.distributionAgent = distributionAgent;
		this.category = MapperUtils.map(category, CategoryResponse.class);
		this.count = count;
		this.created = created;
	}

	public DistributionHistoryResponse()
	{
	}
}
