package com.ptweb.production.payload.response;

import com.ptweb.production.entity.Category;
import com.ptweb.production.entity.Product;
import com.ptweb.production.util.MapperUtils;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */
@Getter
@Setter
public class ProductWarrantyResponse
{
	private long id;
	private long productId;
	private String code;
	private CategoryResponse category;
	private String errorMessage;

	public ProductWarrantyResponse(long id, Product product, Category category, String errorMessage)
	{
		this.id = id;
		this.productId = product.getId();
		this.code = product.getCode();
		this.category = MapperUtils.map(category, CategoryResponse.class);
		this.errorMessage = errorMessage;
	}

	public ProductWarrantyResponse()
	{
	}
}
