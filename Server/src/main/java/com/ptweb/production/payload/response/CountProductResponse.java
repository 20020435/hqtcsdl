package com.ptweb.production.payload.response;

import com.ptweb.production.entity.Category;
import com.ptweb.production.util.MapperUtils;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */
@Getter
@Setter
public class CountProductResponse
{
	private CategoryResponse category;
	private long count;

	public CountProductResponse()
	{
	}

	public CountProductResponse(Category category, long count)
	{
		this.category = MapperUtils.map(category, CategoryResponse.class);
		this.count = count;
	}
}
