package com.ptweb.production.payload.response;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

@Getter
@Setter
public class CategoryResponse
{
	private Long id;
	private String code;
	private String name;
	private String image;
	private String shortDescription;
	private String content;
	private Integer price;
	private String engineType;
	private String gear;
	private String cylinderCapacity;
	private String maximumOutput;
	private String maximumTorque;
	private String maxSpeed;
	private String tankCapacity;
	private String fuelPumpSystem;
}
