package com.ptweb.production.schedule;

import com.ptweb.production.repository.BranchRepository;
import com.ptweb.production.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.Duration;
import java.time.LocalDate;
import java.util.Date;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */
@Configuration
@EnableScheduling
public class GeneratePartition {
    public static String[] tables = {"production_history"};
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    BranchRepository partitionRepository;

    @Scheduled(cron =  "0 0 1 * * *")
    public void createPartition()
    {
        Date date = new Date(System.currentTimeMillis() + Duration.ofDays(1).toMillis());
        for(String table : tables)
        {
            String countSql = "SELECT count(*) FROM INFORMATION_SCHEMA.PARTITIONS " +
                    "WHERE TABLE_SCHEMA = 'production_move' AND TABLE_NAME = '"+ table +"' and PARTITION_NAME is not null";
            int rowCount = jdbcTemplate.queryForObject(countSql, Integer.class);
            if(rowCount == 0)
            {
                String sql = "ALTER TABLE "+ table +" PARTITION BY RANGE (TO_DAYS(createddate)) " +
                        "(PARTITION p" + DateUtils.getDateByFormat("yyyyMMdd", date) +
                        " VALUES LESS THAN (TO_DAYS('" + DateUtils.getDateByFormat("yyyy-MM-dd", date) + " 00:00:00')))";
                jdbcTemplate.execute(sql);
            } else if(rowCount > 0)
            {

                String sql = "ALTER TABLE "+ table +" ADD PARTITION " +
                        "(PARTITION p" + DateUtils.getDateByFormat("yyyyMMdd", date) +
                        " VALUES LESS THAN (TO_DAYS('" + DateUtils.getDateByFormat("yyyy-MM-dd", date) + " 00:00:00')))";
                jdbcTemplate.execute(sql);
            }
        }
    }
}
