package com.ptweb.production.schedule;

import com.ptweb.production.util.DateUtils;
import com.ptweb.production.util.LoggerUtils;
import com.ptweb.production.util.PropertiesLoader;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.sql.DataSource;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableScheduling
public class BackupData
{
    @Autowired
    DataSource dataSource;

    @Scheduled(cron =  "0 0 1 * * *")
    public void scheduleFixedDelayTask() throws Exception
    {
        String fileName = "backup" + DateUtils.getDateByFormat("yyyyMMdd", new Date()) + ".sql";
        Path sqlFile = Paths.get("BackUpDB\\" + fileName);
        ByteArrayOutputStream stdErr = new ByteArrayOutputStream();
        OutputStream stdOut = new BufferedOutputStream(Files.newOutputStream(sqlFile, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING));
        try (stdErr; stdOut)
        {
            ExecuteWatchdog watchdog = new ExecuteWatchdog(TimeUnit.HOURS.toMillis(1));

            DefaultExecutor defaultExecutor = new DefaultExecutor();
            defaultExecutor.setWatchdog(watchdog);
            defaultExecutor.setStreamHandler(new PumpStreamHandler(stdOut, stdErr));

            CommandLine commandLine = new CommandLine("C:\\Program Files\\MySQL\\MySQL Server 8.0\\bin\\mysqldump");
            commandLine.addArgument("-h" + "127.0.0.1"); // server
            commandLine.addArgument("-P" + "3307"); // port
            commandLine.addArgument("-u" + PropertiesLoader.getProperty("spring.datasource.username")); // username
            commandLine.addArgument("-p" + PropertiesLoader.getProperty("spring.datasource.password")); // password
            commandLine.addArgument("production_move"); // database

            LoggerUtils.log("Exporting SQL data...");

            int exitCode = defaultExecutor.execute(commandLine);

            if(defaultExecutor.isFailure(exitCode) && watchdog.killedProcess()) {
                LoggerUtils.error("timeout...");
            }

            LoggerUtils.log("SQL data export completed: exitCode={}" + exitCode + " sqlFile= " + sqlFile.toString());

        } catch (Exception e) {
            LoggerUtils.error("SQL data export exception: " + e.getMessage());
            LoggerUtils.error("std err: " + System.lineSeparator() + stdErr.toString());
        }
    }
}
