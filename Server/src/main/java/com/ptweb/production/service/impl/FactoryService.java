package com.ptweb.production.service.impl;

import com.ptweb.production.entity.*;
import com.ptweb.production.model.CustomPageRequest;
import com.ptweb.production.payload.request.ExportProductRequest;
import com.ptweb.production.payload.request.FactoryStatisticalRequest;
import com.ptweb.production.payload.response.*;
import com.ptweb.production.repository.*;
import com.ptweb.production.service.IFactoryService;
import com.ptweb.production.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

@Service
public class FactoryService implements IFactoryService
{
	@Autowired
	FactoryRepository factoryRepository;
	@Autowired
	ProductRepository productRepository;
	@Autowired
	DistributionHistoryRepository historyRepository;
	@Autowired
	CategoryRepository categoryRepository;
	@Autowired
	WarrantyHistoryRepository warrantyHistoryRepository;

	@Override
	public String exportProduct(ExportProductRequest request)
	{
		long factoryId = factoryRepository.getFactoryIdByBranchId(SecurityUtils.getPrincipal().getBranchId());
		Category category = categoryRepository.findOneById(request.getCategoryId());
		List<Product> list = factoryRepository.getProductToExportByCount(factoryId,
				ProductStatus.NEWLY_PRODUCED,
				category, new CustomPageRequest(0, (int) request.getCount(),
						Sort.by(Sort.Direction.ASC, "code")));
		StringBuilder productCodeList = new StringBuilder();
		for (Product product : list)
		{
			if (productCodeList.length() > 0)
				productCodeList.append(",");
			productCodeList.append(product.getCode());
			product.setProductStatus(ProductStatus.COMING_DISTRIBUTION);
		}
		String message = "Export " + list.size() + " product done";
		DistributionHistory history = new DistributionHistory();
		history.setCount((long) list.size());
		history.setCategoryId(request.getCategoryId());
		history.setStatus(0L);
		history.setDistributionAgentId(request.getDistributionAgentId());
		history.setProductCodeList(productCodeList.toString());
		history.setMessage(message);
		history.setFactoryId(factoryId);
		productRepository.saveAll(list);
		historyRepository.save(history);
		return message;
	}

	@Override
	public List<CountProductResponse> getCountProduct()
	{
		long factoryId = factoryRepository.getFactoryIdByBranchId(SecurityUtils.getPrincipal().getBranchId());
		return factoryRepository.getCountProductByCategory(factoryId);
	}

	@Override
	public List<CountProductResponse> statisticalProductByStatus(FactoryStatisticalRequest request)
	{
		long factoryId = factoryRepository.getFactoryIdByBranchId(SecurityUtils.getPrincipal().getBranchId());
		List<ProductStatus> statuses = new LinkedList<>();
		for (long id : request.getStatuses())
			statuses.add(ProductStatus.of(id));
		return factoryRepository.statisticalProductByStatus(factoryId, statuses,
				request.getDateFrom(), request.getDateTo());
	}

	@Override
	public List<CountProductResponse> statisticalSellProduct(FactoryStatisticalRequest request)
	{
		long factoryId = factoryRepository.getFactoryIdByBranchId(SecurityUtils.getPrincipal().getBranchId());
		return factoryRepository.statisticalSellProduct(factoryId, request.getDateFrom(), request.getDateTo());
	}

	@Override
	public List<ErrorRateResponse> statisticalErrorProduct(FactoryStatisticalRequest request)
	{
		long factoryId = factoryRepository.getFactoryIdByBranchId(SecurityUtils.getPrincipal().getBranchId());
		return factoryRepository.statisticalErrorProduct(factoryId, request.getDateFrom(), request.getDateTo());
	}

	@Override
	public List<ProductionHistoryResponse> getProductionHistory()
	{
		long factoryId = factoryRepository.getFactoryIdByBranchId(SecurityUtils.getPrincipal().getBranchId());
		return factoryRepository.getProductionHistory(factoryId);
	}

	@Override
	public List<DistributionHistoryResponse> getDistributionHistory()
	{
		long factoryId = factoryRepository.getFactoryIdByBranchId(SecurityUtils.getPrincipal().getBranchId());
		return factoryRepository.getDistributionHistory(factoryId);
	}

	@Override
	public List<ProductWarrantyResponse> getErrorProduct()
	{
		long factoryId = factoryRepository.getFactoryIdByBranchId(SecurityUtils.getPrincipal().getBranchId());
		return factoryRepository.getErrorProduct(factoryId);
	}

	@Override
	@Transactional
	public String updateErrorProduct(long id)
	{
		WarrantyHistory warrantyHistory = warrantyHistoryRepository.findOneById(id);
		warrantyHistory.setStatus(6L);
		warrantyHistoryRepository.save(warrantyHistory);
		Product product = productRepository.findOneById(warrantyHistory.getProductId());
		product.setProductStatus(ProductStatus.ERROR_ALREADY_SENT_TO_FACTORY);
		return "success!!";
	}
}
