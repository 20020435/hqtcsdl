package com.ptweb.production.service.impl;

import com.ptweb.production.entity.Category;
import com.ptweb.production.payload.request.CategoryRequest;
import com.ptweb.production.payload.response.CategoryResponse;
import com.ptweb.production.repository.CategoryRepository;
import com.ptweb.production.service.ICategoryService;
import com.ptweb.production.util.FileUploadUtils;
import com.ptweb.production.util.MapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class CategoryService implements ICategoryService
{
	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private GoogleDiverService googleDiverService;

	@Override
	public CategoryResponse findOneById(Long id)
	{
		Category category = categoryRepository.findOneById(id);
		return MapperUtils.map(category, CategoryResponse.class);
	}

	@Override
	public CategoryResponse findOneByCode(String code)
	{
		Category category = categoryRepository.findOneByCode(code);
		return MapperUtils.map(category, CategoryResponse.class);
	}

	@Override
	public CategoryResponse save(CategoryRequest request) throws Exception
	{
		String link = FileUploadUtils.uploadFileToGG(googleDiverService, request.getImage());
		Category category = MapperUtils.map(request, Category.class);
		category.setImage(link);
		category = categoryRepository.save(category);
		return MapperUtils.map(category, CategoryResponse.class);
	}

	@Override
	public List<CategoryResponse> findAll()
	{
		List<Category> list = categoryRepository.findAll();
		return MapperUtils.mapList(list, CategoryResponse.class);
	}

	@Override
	public CategoryResponse update(CategoryRequest request) throws Exception
	{
		String link = FileUploadUtils.uploadFileToGG(googleDiverService, request.getImage());
		Category category = MapperUtils.map(request, Category.class);
		category.setId(request.getId());
		category.setImage(link);
		category = categoryRepository.save(category);
		return MapperUtils.map(category, CategoryResponse.class);
	}

	@Override
	public void delete(long id)
	{
		categoryRepository.deleteById(id);
	}
}
