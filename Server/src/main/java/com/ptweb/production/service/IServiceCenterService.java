package com.ptweb.production.service;

import com.ptweb.production.entity.RefreshToken;
import com.ptweb.production.payload.request.ServiceCenterStatisticalRequest;
import com.ptweb.production.payload.request.WarehouseRequest;
import com.ptweb.production.payload.response.ProductWarrantyResponse;
import com.ptweb.production.payload.response.ServiceCenterResponse;
import com.ptweb.production.payload.response.StatisticalResponse;
import com.ptweb.production.payload.response.WarehouseResponse;

import java.util.List;
import java.util.Optional;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */
public interface IServiceCenterService
{
	List<ServiceCenterResponse> getAll();

	List<ProductWarrantyResponse> getAllOrderWarranty();

	List<ProductWarrantyResponse> getAllWarranty();

	String updateOrderWarranty(long id);

	String updateWarrantyToError(long id);

	String updateWarrantyToDone(long id);

	List<StatisticalResponse> statisticalProduct(ServiceCenterStatisticalRequest request);
}
