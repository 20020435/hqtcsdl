package com.ptweb.production.service.impl;

import com.ptweb.production.entity.*;
import com.ptweb.production.payload.request.AddProductRequest;
import com.ptweb.production.payload.response.ProductResponse;
import com.ptweb.production.repository.*;
import com.ptweb.production.service.IProductService;
import com.ptweb.production.util.MapperUtils;
import com.ptweb.production.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

@Service
public class ProductService implements IProductService
{
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private FactoryRepository factoryRepository;
	@Autowired
	private WareHouseRepository wareHouseRepository;
	@Autowired
	private ProductionHistoryRepository productionHistoryRepository;

	@Override
	public List<ProductResponse> findAll(Pageable pageable)
	{
		return MapperUtils.mapList(productRepository.findAll(pageable).getContent(), ProductResponse.class);
	}

	@Override
	public List<ProductResponse> findAllByCategory(long categoryId, Pageable pageable)
	{
		return MapperUtils.mapList(productRepository.findByCategoryId(categoryId, pageable), ProductResponse.class);
	}

	@Override
	@Transactional
	public void deleteItemsByIds(long[] ids)
	{
		for (long id : ids)
			productRepository.deleteById(id);
	}

	@Override
	@Transactional
	public void updateProduct(Product entity)
	{
		productRepository.save(entity);
	}

	@Override
	public long countProducts()
	{
		return productRepository.countProducts();
	}

	@Override
	public List<ProductResponse> addProduct(AddProductRequest request)
	{
		List<Product> list = new LinkedList<>();
		Category category = categoryRepository.findOneById((long) request.getCategoryId());
		String categoryCode = category.getCode();
		long totalProduct = productRepository.countProductsByCategoryId(request.getCategoryId());
		WareHouse wareHouse = findWareHouse();
		long factoryId = factoryRepository.getFactoryIdByBranchId(SecurityUtils.getPrincipal().getBranchId());
		for (int i = 0; i < request.getCount(); i++)
		{
			Product product = new Product();
			product.setCategory(category);
			product.setCode(categoryCode + "-" + (totalProduct + i + 1));
			product.setWareHouse(wareHouse);
			product.setFactoryId(factoryId);
			product.setProductStatus(ProductStatus.NEWLY_PRODUCED);
			list.add(product);
		}
		list = productRepository.saveAll(list);
		ProductionHistory productionHistory = new ProductionHistory();
		productionHistory.setFactoryId(factoryId);
		productionHistory.setCategoryId(category.getId());
		productionHistory.setCount((long) request.getCount());
		productionHistoryRepository.save(productionHistory);
		return MapperUtils.mapList(list, ProductResponse.class);
	}

	@Override
	public ProductResponse findOneById(Long id)
	{
		return MapperUtils.map(productRepository.findOneById(id), ProductResponse.class);
	}

	private WareHouse findWareHouse()
	{
		UserDetailsImpl userDetails = SecurityUtils.getPrincipal();
		long wareHouseId = factoryRepository.getWareHouseIdByBranchId(userDetails.getBranchId());
		return wareHouseRepository.findOneById(wareHouseId);
	}
}
