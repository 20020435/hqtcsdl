package com.ptweb.production.service.impl;

import com.ptweb.production.entity.Branch;
import com.ptweb.production.entity.RefreshToken;
import com.ptweb.production.entity.User;
import com.ptweb.production.model.ModelMapper;
import com.ptweb.production.payload.request.AuthRequest;
import com.ptweb.production.payload.request.UserRequest;
import com.ptweb.production.payload.response.UserResponse;
import com.ptweb.production.repository.BranchRepository;
import com.ptweb.production.repository.RefreshTokenRepository;
import com.ptweb.production.repository.RoleRepository;
import com.ptweb.production.repository.UserRepository;
import com.ptweb.production.service.IUserService;
import com.ptweb.production.util.MapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserService implements IUserService
{
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private BranchRepository branchRepository;

	@Autowired
	private RefreshTokenRepository refreshTokenRepository;

	@Override
	@Transactional
	public UserResponse save(UserRequest request)
	{
		User user = userRepository.findOneByUserName(request.getUsername());
		if (user == null)
		{
			user = new User();
			Branch branch = branchRepository.findOneById(request.getBranchId());
			user.setUserName(request.getUsername());
			user.setPassword(new BCryptPasswordEncoder().encode(request.getPassword()));
			user.setFullName(request.getFullName());
			user.setAddress(user.getAddress());
			user.setPhoneNumber(request.getPhoneNumber());
			user.setBranch(branch);
			String role = branch.getRole();
			if(role.equalsIgnoreCase("service-center"))
				role = "SERVICE_CENTER";
			if(role.equalsIgnoreCase("distribution-agent"))
				role = "DISTRIBUTION_AGENT";
			user.setRole(roleRepository.findOneByCodeContainingIgnoreCase(role));
			if (userRepository.countUser() == 0)
				user.setRole(roleRepository.findOneByCodeContainingIgnoreCase("ADMIN"));
			user.setRefreshToken(refreshTokenRepository.save(new RefreshToken()));
			return MapperUtils.map(userRepository.save(user), UserResponse.class);
		}
		return null;
	}

	@Override
	@Transactional
	public UserResponse update(UserRequest request)
	{
		User user = userRepository.findOneById(request.getId());
		user = MapperUtils.map(request, User.class);
		user.setId(request.getId());
		return MapperUtils.map(userRepository.save(user), UserResponse.class);
	}

	@Override
	public UserResponse findOneById(Long id)
	{
		User user = userRepository.findOneById(id);
		UserResponse response = MapperUtils.map(user, UserResponse.class);
		response.setRole(user.getRole().getCode());
		return response;
	}

	@Override
	public UserResponse findOneByUserName(String name)
	{
		return MapperUtils.map(userRepository.findOneByUserName(name), UserResponse.class);
	}

	@Override
	public long countUser()
	{
		return userRepository.countUser();
	}

	@Override
	public List<UserResponse> findAll(Pageable pageable)
	{
		return MapperUtils.mapList(userRepository.findAll(pageable).getContent(), UserResponse.class);
	}

	@Override
	@Transactional
	public void deleteUserByIds(long[] ids)
	{
		for (long id : ids)
		{
			userRepository.deleteById(id);
		}
	}

	@Override
	public void delete(long id)
	{
		userRepository.deleteById(id);
	}
}
