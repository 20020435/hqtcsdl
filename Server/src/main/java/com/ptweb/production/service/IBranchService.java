package com.ptweb.production.service;

import com.ptweb.production.payload.request.BranchRequest;
import com.ptweb.production.payload.request.CategoryRequest;
import com.ptweb.production.payload.response.BranchResponse;
import com.ptweb.production.payload.response.CategoryResponse;
import com.ptweb.production.payload.response.UserResponse;

import java.util.List;

public interface IBranchService
{
	BranchResponse findOneById(Long id);

	BranchResponse save(BranchRequest request);

	List<BranchResponse> findAll();

	List<UserResponse> findAllUser(long branchId);

	BranchResponse update(BranchRequest request);

	void delete(long id);
}
