package com.ptweb.production.service.impl;

import com.ptweb.production.entity.*;
import com.ptweb.production.model.CustomPageRequest;
import com.ptweb.production.payload.request.DistributionStatisticalRequest;
import com.ptweb.production.payload.request.PaymentRequest;
import com.ptweb.production.payload.request.ProductWarrantyRequest;
import com.ptweb.production.payload.request.WarehouseRequest;
import com.ptweb.production.payload.response.*;
import com.ptweb.production.repository.*;
import com.ptweb.production.service.IDistributionAgentService;
import com.ptweb.production.util.MapperUtils;
import com.ptweb.production.util.SecurityUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Service
public class DistributionAgentService implements IDistributionAgentService
{
	@Autowired
	WareHouseRepository wareHouseRepository;
	@Autowired
	DistributionAgentRepository agentRepository;
	@Autowired
	AgentWareHouseRepository agentWareHouseRepository;
	@Autowired
	TransactionHistoryRepository transactionRepository;
	@Autowired
	ProductRepository productRepository;
	@Autowired
	DistributionHistoryRepository distributionHistoryRepository;
	@Autowired
	WarrantyHistoryRepository warrantyHistoryRepository;
	@Autowired
	CategoryRepository categoryRepository;

	@Override
	public WarehouseResponse addWarehouse(WarehouseRequest request)
	{
		WareHouse wareHouse = new WareHouse();
		wareHouse.setAddress(request.getAddress());
		wareHouse.setName(request.getName());
		wareHouse = wareHouseRepository.save(wareHouse);
		AgentWareHouse agentWareHouse = new AgentWareHouse();
		agentWareHouse.setWarehouseId(wareHouse.getId());
		agentWareHouse.setDistributionId(getDistributionId());
		agentWareHouseRepository.save(agentWareHouse);
		return MapperUtils.map(wareHouse, WarehouseResponse.class);
	}

	@Override
	public List<WarehouseResponse> findAllWarehouse()
	{
		List<WareHouse> list = wareHouseRepository.findAllWithDistributionAgentId(getDistributionId());
		return MapperUtils.mapList(list, WarehouseResponse.class);
	}

	@Override
	@Transactional
	public PaymentResponse addPayment(PaymentRequest request)
	{
		List<Product> list = agentRepository.findFirstByWarehouseId(request.getWarehouseId(),
				request.getCategoryId(), new CustomPageRequest(0, 1));
		if (list == null || list.isEmpty())
			return null;
		Product product = list.get(0);
		TransactionHistory transactionHistory = new TransactionHistory();
		transactionHistory.setDistributionAgentId(getDistributionId());
		transactionHistory.setCustomerName(request.getCustomerName());
		transactionHistory.setProductId(product.getId());
		transactionRepository.save(transactionHistory);
		product.setProductStatus(ProductStatus.SOLD);
		Long warrantyPeriod = product.getCategory().getWarrantyPeriod();
		Date warrantyPeriodDate = DateUtils.addMonths(new Date(), Math.toIntExact(warrantyPeriod));
		product.setWarrantyPeriodDate(warrantyPeriodDate);
		productRepository.save(product);
		PaymentResponse response = new PaymentResponse();
		response.setProductCode(product.getCode());
		return response;
	}

	@Override
	public String updateOrder(long id, long warehouseId)
	{
		DistributionHistory history = distributionHistoryRepository.findOneById(id);
		history.setStatus(1L);
		distributionHistoryRepository.save(history);
		List<Product> list = new LinkedList<>();
		for (String s : history.getProductCodeList().split(","))
		{
			Product product = productRepository.findOneByCodeIgnoreCase(s);
			product.setProductStatus(ProductStatus.AT_DISTRIBUTION);
			product.setDistributionAgentId(history.getDistributionAgentId());
			product.setWareHouse(wareHouseRepository.findOneById(warehouseId));
			list.add(product);
		}
		productRepository.saveAll(list);
		return "success";
	}

	@Override
	public List<DistributionHistoryResponseTwo> getAllOrder()
	{
		return distributionHistoryRepository.findAllByDistributionId(getDistributionId());
	}

	@Override
	public String addWarranty(ProductWarrantyRequest request)
	{
		WarrantyHistory history = new WarrantyHistory();
		history.setDistributionAgentId(getDistributionId());
		history.setErrorMessage(request.getErrorMessage());
		Product product = productRepository.findOneByCodeIgnoreCase(request.getProductCode());
		if (!product.getProductStatus().equals(ProductStatus.SOLD) &&
				!product.getProductStatus().equals(ProductStatus.WARRANTY_RETURNED_TO_CUSTOMER))
			return "Failed";
		Date warrantyPeriod = product.getWarrantyPeriodDate();
		if (new Date().after(warrantyPeriod))
			return "Out of warranty period date";

		history.setProductId(product.getId());
		history.setServiceCenterId(request.getServiceCenterId());
		history.setStatus(0L);
		product.setProductStatus(ProductStatus.ERROR_NEED_WARRANTY);
		product.setWarrantyCount(product.getWarrantyCount() + 1);
		warrantyHistoryRepository.save(history);
		productRepository.save(product);
		return "Success!!!";
	}

	@Override
	public List<ProductWarehouseResponse> countProduct(long categoryId)
	{
		return agentRepository.getProductInWarehouse(getDistributionId(), categoryId);
	}

	@Override
	public List<ProductWarrantyResponse> getWarrantyError()
	{
		return agentRepository.getWarrantyError(getDistributionId());
	}

	@Override
	public List<ProductWarrantyResponse> getWarrantyDone()
	{
		return agentRepository.getWarrantyDone(getDistributionId());
	}

	@Override
	public String updateWarrantyDone(long id)
	{
		WarrantyHistory history = warrantyHistoryRepository.findOneById(id);
		if (history == null || history.getStatus() != 2)
			return "Failed";
		history.setStatus(5L);
		Product product = productRepository.findOneById(history.getProductId());
		if (product == null || !product.getProductStatus().equals(ProductStatus.WARRANTY_DONE))
			return "Failed";
		product.setProductStatus(ProductStatus.WARRANTY_RETURNED_TO_CUSTOMER);
		warrantyHistoryRepository.save(history);
		productRepository.save(product);
		return "update success";
	}

	@Override
	public String updateWarrantyError(long id)
	{
		WarrantyHistory history = warrantyHistoryRepository.findOneById(id);
		if (history == null || history.getStatus() != 3)
			return "Failed";
		history.setStatus(4L);
		Product product = productRepository.findOneById(history.getProductId());
		if (product == null || !product.getProductStatus().equals(ProductStatus.ERROR_NEED_RETURN_FACTORY))
			return "Failed";
		history.setFactoryId(product.getFactoryId());
		product.setProductStatus(ProductStatus.ERROR_ALREADY_SENT_TO_FACTORY);
		warrantyHistoryRepository.save(history);
		productRepository.save(product);
		return "update success";
	}

	@Override
	public List<CountProductResponse> statisticalSellProduct(DistributionStatisticalRequest request)
	{
		List<List<Long>> list = agentRepository.statisticalSellProduct(getDistributionId(),
				request.getDateFrom().getTime(), request.getDateTo().getTime());
		List<CountProductResponse> responses = new LinkedList<>();
		for (List<Long> longList : list)
		{
			Category category = categoryRepository.findOneById(longList.get(0));
			CountProductResponse countProductResponse = new CountProductResponse(category, longList.get(1));
			responses.add(countProductResponse);
		}
		return responses;
	}

	@Override
	public List<ListWarrantyResponse> getAllWarranty()
	{
		return agentRepository.getAllWarranty(getDistributionId());
	}

	@Override
	public WarehouseResponse updateWarehouse(WarehouseRequest request)
	{
		WareHouse wareHouse = wareHouseRepository.findOneById(request.getId());
		wareHouse.setAddress(request.getAddress());
		wareHouse.setName(request.getName());
		wareHouse = wareHouseRepository.save(wareHouse);
		return MapperUtils.map(wareHouse, WarehouseResponse.class);
	}

	@Override
	public void deleteWarehouse(long id)
	{
		wareHouseRepository.deleteById(id);
	}

	@Override
	public List<DistributionAgentResponse> getAll()
	{
		List<DistributionAgent> list = agentRepository.findAll();
		return MapperUtils.mapList(list, DistributionAgentResponse.class);
	}

	public Long getDistributionId()
	{
		return agentRepository.getDistributionAgentIdByBranchId(SecurityUtils.getPrincipal().getBranchId());
	}
}
