package com.ptweb.production.service.impl;

import com.ptweb.production.entity.*;
import com.ptweb.production.payload.request.AdminStatisticalRequest;
import com.ptweb.production.payload.request.BranchRequest;
import com.ptweb.production.payload.response.BranchResponse;
import com.ptweb.production.payload.response.StatisticalResponse;
import com.ptweb.production.repository.*;
import com.ptweb.production.service.IAdminService;
import com.ptweb.production.service.IBranchService;
import com.ptweb.production.util.MapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminService implements IAdminService
{
	@Autowired
	private ProductRepository productRepository;

	@Override
	public List<StatisticalResponse> statisticalProductStatus(AdminStatisticalRequest request)
	{
		return productRepository.statisticalProductStatus(request.getIds(),request.getDateFrom(),request.getDateTo());
	}

	@Override
	public List<StatisticalResponse> statisticalProductFactory(AdminStatisticalRequest request)
	{
		return productRepository.statisticalProductFactory(request.getIds(),request.getDateFrom(),request.getDateTo());
	}

	@Override
	public List<StatisticalResponse> statisticalProductTransaction(AdminStatisticalRequest request)
	{
		return productRepository.statisticalProductTransaction(request.getIds(),request.getDateFrom(),request.getDateTo());
	}

	@Override
	public List<StatisticalResponse> statisticalProductWarranty(AdminStatisticalRequest request)
	{
		return productRepository.statisticalProductWarranty(request.getIds(),request.getDateFrom(),request.getDateTo());
	}
}
