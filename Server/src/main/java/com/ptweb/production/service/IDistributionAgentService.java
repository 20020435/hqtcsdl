package com.ptweb.production.service;

import com.ptweb.production.payload.request.DistributionStatisticalRequest;
import com.ptweb.production.payload.request.PaymentRequest;
import com.ptweb.production.payload.request.ProductWarrantyRequest;
import com.ptweb.production.payload.request.WarehouseRequest;
import com.ptweb.production.payload.response.*;

import java.util.List;

public interface IDistributionAgentService
{
	List<DistributionAgentResponse> getAll();

	WarehouseResponse addWarehouse(WarehouseRequest request);

	List<WarehouseResponse> findAllWarehouse();

	PaymentResponse addPayment(PaymentRequest request);

	String updateOrder(long id, long warehouseId);

	List<DistributionHistoryResponseTwo> getAllOrder();

	String addWarranty(ProductWarrantyRequest request);

	List<ProductWarehouseResponse> countProduct(long categoryId);

	List<ProductWarrantyResponse> getWarrantyError();

	List<ProductWarrantyResponse> getWarrantyDone();

	String updateWarrantyDone(long id);

	String updateWarrantyError(long id);

	List<CountProductResponse> statisticalSellProduct(DistributionStatisticalRequest request);

	List<ListWarrantyResponse> getAllWarranty();

	WarehouseResponse updateWarehouse(WarehouseRequest request);

	void deleteWarehouse(long id);
}
