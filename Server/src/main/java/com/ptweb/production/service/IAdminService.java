package com.ptweb.production.service;

import com.ptweb.production.payload.request.AdminStatisticalRequest;
import com.ptweb.production.payload.request.UserRequest;
import com.ptweb.production.payload.response.StatisticalResponse;
import com.ptweb.production.payload.response.UserResponse;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IAdminService
{
    List<StatisticalResponse> statisticalProductStatus(AdminStatisticalRequest request);

    List<StatisticalResponse> statisticalProductFactory(AdminStatisticalRequest request);

    List<StatisticalResponse> statisticalProductTransaction(AdminStatisticalRequest request);

    List<StatisticalResponse> statisticalProductWarranty(AdminStatisticalRequest request);
}
