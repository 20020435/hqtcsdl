package com.ptweb.production.service.impl;

import com.ptweb.production.entity.*;
import com.ptweb.production.model.CustomPageRequest;
import com.ptweb.production.payload.request.ExportProductRequest;
import com.ptweb.production.payload.request.FactoryStatisticalRequest;
import com.ptweb.production.payload.request.ServiceCenterStatisticalRequest;
import com.ptweb.production.payload.request.WarehouseRequest;
import com.ptweb.production.payload.response.*;
import com.ptweb.production.repository.*;
import com.ptweb.production.service.IFactoryService;
import com.ptweb.production.service.IServiceCenterService;
import com.ptweb.production.util.MapperUtils;
import com.ptweb.production.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

@Service
public class ServiceCenterService implements IServiceCenterService
{
	@Autowired
	ServiceCenterRepository serviceCenterRepository;
	@Autowired
	WarrantyHistoryRepository warrantyHistoryRepository;
	@Autowired
	ProductRepository productRepository;

	public Long getServiceCenterId()
	{
		return serviceCenterRepository.getServiceCenterIdByBranchId(SecurityUtils.getPrincipal().getBranchId());
	}

	@Override
	public List<ServiceCenterResponse> getAll()
	{
		List<ServiceCenter> serviceCenters = serviceCenterRepository.findAll();
		return MapperUtils.mapList(serviceCenters, ServiceCenterResponse.class);
	}

	@Override
	public List<ProductWarrantyResponse> getAllOrderWarranty()
	{
		return serviceCenterRepository.getAllOrderWarranty(getServiceCenterId());
	}

	@Override
	public List<ProductWarrantyResponse> getAllWarranty()
	{
		return serviceCenterRepository.getAllWarranty(getServiceCenterId());
	}

	@Override
	public String updateOrderWarranty(long id)
	{
		WarrantyHistory history = warrantyHistoryRepository.findOneById(id);
		if (history == null || history.getStatus() != 0)
			return "Failed";
		history.setStatus(1L);
		Product product = productRepository.findOneById(history.getProductId());
		if (product == null || !product.getProductStatus().equals(ProductStatus.ERROR_NEED_WARRANTY))
			return "Failed";
		product.setProductStatus(ProductStatus.UNDER_WARRANTY);
		warrantyHistoryRepository.save(history);
		productRepository.save(product);
		return "update success";
	}

	@Override
	public String updateWarrantyToError(long id)
	{
		WarrantyHistory history = warrantyHistoryRepository.findOneById(id);
		if (history == null || history.getStatus() != 1)
			return "Failed";
		history.setStatus(3L);
		Product product = productRepository.findOneById(history.getProductId());
		if (product == null || !product.getProductStatus().equals(ProductStatus.UNDER_WARRANTY))
			return "Failed";
		product.setProductStatus(ProductStatus.ERROR_NEED_RETURN_FACTORY);
		warrantyHistoryRepository.save(history);
		productRepository.save(product);
		return "update success";
	}

	@Override
	public String updateWarrantyToDone(long id)
	{
		WarrantyHistory history = warrantyHistoryRepository.findOneById(id);
		history.setStatus(2L);
		Product product = productRepository.findOneById(history.getProductId());
		if (product == null || product.getProductStatus().equals(ProductStatus.UNDER_WARRANTY))
		product.setProductStatus(ProductStatus.WARRANTY_DONE);
		warrantyHistoryRepository.save(history);
		productRepository.save(product);
		return "update success";
	}

	@Override
	public List<StatisticalResponse> statisticalProduct(ServiceCenterStatisticalRequest request)
	{
		List<StatisticalResponse> list = new LinkedList<>();
		long centerId = getServiceCenterId();
		list.add(serviceCenterRepository.statisticalProductDone(centerId, request.getDateFrom(), request.getDateTo()));
		list.add(serviceCenterRepository.statisticalProductError(centerId, request.getDateFrom(), request.getDateTo()));
		return list;
	}
}
