package com.ptweb.production.service;

import com.ptweb.production.payload.request.BranchRequest;
import com.ptweb.production.payload.response.BranchResponse;

import java.util.List;

public interface IStatisticalService
{
	BranchResponse findOneById(Long id);
}
