package com.ptweb.production.service.impl;

import com.google.api.client.http.FileContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.ptweb.production.util.PropertiesLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class GoogleDiverService
{
	@Autowired
	private Drive googleDrive;

	private List<File> getAllGoogleDriveFiles() throws IOException
	{
		FileList result = googleDrive.files().list()
				.setFields("nextPageToken, files(id, name, parents, mimeType)")
				.execute();
		return result.getFiles();
	}


	public String addGoogleDriveFiles(java.io.File file, String name) throws IOException
	{
		File newGGDriveFile = new File();
		newGGDriveFile.setParents(List.of(PropertiesLoader.getProperty("ggdriver.api.idFolder"))).setName(name);
		FileContent mediaContent = new FileContent("image/jpg", file);
		File newFile = googleDrive.files().create(newGGDriveFile, mediaContent).setFields("id,webViewLink").execute();
		return editLinkImage(newFile.getWebViewLink());
	}

	public String editLinkImage(String link)
	{
		String[] strings = link.split("/");
		String newLink = "https://drive.google.com/uc?export=view&id=" + strings[5];
		return newLink;
	}
}
