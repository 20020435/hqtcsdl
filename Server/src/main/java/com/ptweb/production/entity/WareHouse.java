package com.ptweb.production.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "ware_house")
public class WareHouse extends BaseEntity
{
	@Column(name = "name")
	private String name;

	@Column(name = "address")
	private String address;

	@OneToMany(mappedBy = "wareHouse",cascade = CascadeType.REMOVE)
	private List<Product> products;
}
