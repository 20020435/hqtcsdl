package com.ptweb.production.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

//Lịch sử bảo hành
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "warranty_history",
		indexes = {@Index(columnList = "distribution_agent_id"),
				@Index(columnList = "distribution_agent_id,status"),
				@Index(columnList = "service_center_id"),
				@Index(columnList = "service_center_id,status")})
public class WarrantyHistory extends BasePartitionEntity
{
	@Column(name = "factory_id")
	private Long factoryId;

	@Column(name = "service_center_id")
	private Long serviceCenterId;

	@Column(name = "distribution_agent_id")
	private Long distributionAgentId;

	@Column(name = "product_id")
	private Long productId;

	/*
		0:Đang đưa về trung tâm bảo hành
		1:Đang bảo hành
		2:bảo hành xong trả về đại lý
		3:lỗi nặng cần trả về nhà máy
		4:lỗi nặng đã trả về nhà máy
		5:đã trả khách hàng
	 */
	@Column(name = "status")
	private Long status;

	@Column(name = "error_message")
	private String errorMessage;
}
