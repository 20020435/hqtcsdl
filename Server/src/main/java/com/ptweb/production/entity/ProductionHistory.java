package com.ptweb.production.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

//Lịch sử sản xuất
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "production_history",
		indexes = {@Index(columnList = "factory_id"),
				@Index(columnList = "category_id"),
				@Index(columnList = "factory_id,category_id"),
				@Index(columnList = "category_id,factory_id")})
public class ProductionHistory extends BasePartitionEntity
{
	@Column(name = "factory_id")
	private Long factoryId;

	@Column(name = "category_id")
	private Long categoryId;

	@Column(name = "count")
	private Long count;
}
