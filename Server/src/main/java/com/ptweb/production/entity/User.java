package com.ptweb.production.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user")
public class User extends BaseEntity
{
	@Column(name = "username")
	@JsonProperty(value = "username")
	private String userName;

	@Column(name = "password")
	private String password;

	@Column(name = "fullname")
	private String fullName;

	@Column(name = "phonenumber")
	private String phoneNumber;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "role_id")
	private Role role;

	@ManyToOne
	@JoinColumn(name = "branch_id")
	private Branch branch;

	@Column(name = "image", columnDefinition = "TEXT")
	private String image;

	@Column(name = "address", columnDefinition = "TEXT")
	private String address;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	@NotNull
	@JoinColumn(name = "refresh_token_id")
	private RefreshToken refreshToken;
}
