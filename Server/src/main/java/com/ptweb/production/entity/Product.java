package com.ptweb.production.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "product",
		indexes = {@Index(columnList = "code"),
				@Index(columnList = "status"),
				@Index(columnList = "category_id"),
				@Index(columnList = "ware_house_id"),
				@Index(columnList = "factory_id,status"),
				@Index(columnList = "category_id,status"),
				@Index(columnList = "ware_house_id,category_id,status"),
				@Index(columnList = "distribution_agent_id,category_id,status")})
public class Product extends BasePartitionEntity
{
	@Column(name = "code", unique=true, nullable = false)
	private String code;

	@Column(name = "status")
	@Enumerated(EnumType.ORDINAL)
	private ProductStatus productStatus;

	@Column(name = "factory_id")
	private Long factoryId;

	@Column(name = "service_center_id")
	private Long serviceCenterId;

	@Column(name = "distribution_agent_id")
	private Long distributionAgentId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "category_id")
	private Category category;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ware_house_id")
	private WareHouse wareHouse;

	@Column(name = "warranty_count", columnDefinition="BIGINT default '0'")
	private Long warrantyCount = 0L;

	@Column(name = "warranty_period_date")
	private Date warrantyPeriodDate;
}
