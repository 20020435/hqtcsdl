package com.ptweb.production.entity;

import java.util.stream.Stream;

public enum ProductStatus
{
	NONE(0), NEWLY_PRODUCED(1), COMING_DISTRIBUTION(2), AT_DISTRIBUTION(3), SOLD(4), ERROR_NEED_WARRANTY(5), UNDER_WARRANTY(6),
	WARRANTY_DONE(7), WARRANTY_RETURNED_TO_CUSTOMER(8), ERROR_NEED_RETURN_FACTORY(9), ERROR_ALREADY_SENT_TO_FACTORY(10),
	ERROR_NEED_SUMMON(11), OUT_OF_WARRANTY_PERIOD(12), RETURN_TO_FACTORY(13);

	private final long status;

	private ProductStatus(long status)
	{
		this.status = status;
	}

	public long getStatus()
	{
		return status;
	}

	public static ProductStatus of(long status)
	{
		return Stream.of(ProductStatus.values())
				.filter(p -> p.getStatus() == status)
				.findFirst()
				.orElseThrow(IllegalArgumentException::new);
	}

	public static ProductStatus copy(ProductStatus status) {
		return of(status.getStatus());
	}

	@Override
	public String toString()
	{
		return switch ((int) status)
				{
					case 1 -> "Mới sản xuất";
					case 2 -> "Đang đưa về đại lý";
					case 3 -> "Đang ở đại lý";
					case 4 -> "Đã bán";
					case 5 -> "Lỗi, cần bảo hành";
					case 6 -> "Đang sửa chữa bảo hành";
					case 7 -> "Đã bảo hành xong";
					case 8 -> "Đã trả lại bảo hành";
					case 9 -> "Lỗi, cần trả về nhà máy";
					case 10 -> "Lỗi, đã trả về nhà máy";
					case 11 -> "Lỗi cần triệu hồi";
					case 12 -> "Hết thời gian bảo hành";
					case 13 -> "Trả lại cơ sở sản xuất";
					default -> "Không tìm thấy trạng thái";
				};
	}
}