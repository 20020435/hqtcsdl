package com.ptweb.production.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

//Lịch sử giao dịch
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "transaction_history",
		indexes = {@Index(columnList = "product_id"),
				@Index(columnList = "distribution_agent_id")})
public class TransactionHistory extends BasePartitionEntity
{
	@Column(name = "distribution_agent_id")
	private Long distributionAgentId;

	@Column(name = "product_id")
	private Long productId;

	@Column(name = "customer_name")
	private String customerName;

	@Column(name = "customer_age")
	private Long customerAge;
}
