package com.ptweb.production.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "category")
public class Category extends BaseEntity
{

	@OneToMany(mappedBy = "category", cascade = CascadeType.REMOVE)
	private List<Product> products;

	@Column(name = "code")
	private String code;

	@Column(name = "name")
	private String name;

	@Column(name = "short_description", columnDefinition = "TEXT")
	private String shortDescription;

	@Column(name = "content", columnDefinition = "TEXT")
	private String content;

	@Column(name = "image", columnDefinition = "TEXT")
	private String image;

	@Column(name = "price")
	private Long price;

	@Column(name = "warranty_period", columnDefinition="BIGINT default '24'")
	private Long warrantyPeriod;

	@Column(name = "engine_type")
	private String engineType;

	@Column(name = "gear")
	private String gear;

	@Column(name = "cylinder_capacity")
	private String cylinderCapacity;

	@Column(name = "maximum_output")
	private String maximumOutput;

	@Column(name = "maximum_torque")
	private String maximumTorque;

	@Column(name = "max_speed")
	private String maxSpeed;

	@Column(name = "tank_capacity")
	private String tankCapacity;

	@Column(name = "fuel_pump_system")
	private String fuelPumpSystem;
}