package com.ptweb.production.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "distribution_agent",
		indexes = {@Index(columnList = "branch_id")})
public class DistributionAgent extends BaseEntity
{
	@Column(name = "name")
	private String name;

	@Column(name = "address")
	private String address;

	@Column(name = "branch_id")
	private Long branchId;
}
