package com.ptweb.production.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "agent_ware_house",
		indexes = {@Index(columnList = "distribution_id")})
public class AgentWareHouse extends BaseEntity
{
	@Column(name = "warehouse_id")
	private Long warehouseId;

	@Column(name = "distribution_id")
	private Long distributionId;
}
