package com.ptweb.production.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

//Lịch sử chuyển cho đại lý phân phối
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "distribution_history",
		indexes = {@Index(columnList = "distribution_agent_id"),
				@Index(columnList = "factory_id"),
				@Index(columnList = "distribution_agent_id,status"),
				@Index(columnList = "factory_id,status")})
public class DistributionHistory extends BasePartitionEntity
{
	@Column(name = "factory_id")
	private Long factoryId;

	@Column(name = "distribution_agent_id")
	private Long distributionAgentId;

	@Column(name = "category_id")
	private Long categoryId;

	@Column(name = "count")
	private Long count;

	@Column(name = "product_code_list")
	private String productCodeList;

	@Column(name = "message")
	private String message;

	@Column(name = "status")
	private Long status;
}
