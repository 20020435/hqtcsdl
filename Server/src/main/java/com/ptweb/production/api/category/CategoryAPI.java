package com.ptweb.production.api.category;

import com.ptweb.production.payload.request.CategoryRequest;
import com.ptweb.production.payload.response.MessageResponse;
import com.ptweb.production.service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api/category")
public class CategoryAPI
{
	@Autowired
	ICategoryService categoryService;

	@PostMapping
	@Secured("ROLE_ADMIN")
	public ResponseEntity<?> addCategory(@ModelAttribute CategoryRequest request)
	{
		try
		{
			return ResponseEntity.ok(categoryService.save(request));
		}
		catch (Exception e)
		{
			return ResponseEntity.badRequest().body(new MessageResponse("Failed to add category!!!"));
		}
	}

	@PutMapping
	@Secured("ROLE_ADMIN")
	public ResponseEntity<?> updateCategory(@ModelAttribute CategoryRequest request)
	{
		try
		{
			return ResponseEntity.ok(categoryService.update(request));
		}
		catch (Exception e)
		{
			return ResponseEntity.badRequest().body(new MessageResponse("Failed to update category!!!"));
		}
	}

	@DeleteMapping("/{id}")
	@Secured("ROLE_ADMIN")
	public ResponseEntity<?> deletCategory(@PathVariable("id") long id)
	{
		try
		{
			categoryService.delete(id);
			return ResponseEntity.ok("ok");
		}
		catch (Exception e)
		{
			return ResponseEntity.badRequest().body(new MessageResponse("Failed to update category!!!"));
		}
	}

	@GetMapping("/all")
	@Secured({"ROLE_ADMIN", "ROLE_FACTORY", "ROLE_DISTRIBUTION_AGENT"})
	public ResponseEntity<?> getAllCategory()
	{
		return ResponseEntity.ok(categoryService.findAll());
	}

}