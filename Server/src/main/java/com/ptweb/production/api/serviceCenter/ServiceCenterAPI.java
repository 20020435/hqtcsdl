package com.ptweb.production.api.serviceCenter;

import com.ptweb.production.payload.request.AdminStatisticalRequest;
import com.ptweb.production.service.IAdminService;
import com.ptweb.production.service.IServiceCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */
@CrossOrigin
@RestController
@RequestMapping("/api/service-center")
@Secured("ROLE_SERVICE_CENTER")
public class ServiceCenterAPI
{
	@Autowired
	IServiceCenterService centerService;

	@Secured({"ROLE_FACTORY", "ROLE_ADMIN", "ROLE_SERVICE_CENTER", "ROLE_DISTRIBUTION_AGENT"})
	@GetMapping("/all")
	public ResponseEntity<?> getAll()
	{
		return ResponseEntity.ok(centerService.getAll());
	}

	@Secured("ROLE_SERVICE_CENTER")
	@GetMapping("/order-warranty")
	public ResponseEntity<?> getAllOrderWarranty()
	{
		return ResponseEntity.ok(centerService.getAllOrderWarranty());
	}

	@Secured("ROLE_SERVICE_CENTER")
	@PutMapping("/order-warranty/{id}")
	public ResponseEntity<?> updateOrderWarranty(@PathVariable("id") long id)
	{
		return ResponseEntity.ok(centerService.updateOrderWarranty(id));
	}

	@Secured("ROLE_SERVICE_CENTER")
	@GetMapping("/warranty/all")
	public ResponseEntity<?> getAllWarranty()
	{
		return ResponseEntity.ok(centerService.getAllWarranty());
	}

	@Secured("ROLE_SERVICE_CENTER")
	@PutMapping("/warranty/error/{id}")
	public ResponseEntity<?> updateWarrantyToError(@PathVariable("id") long id)
	{
		return ResponseEntity.ok(centerService.updateWarrantyToError(id));
	}

	@Secured("ROLE_SERVICE_CENTER")
	@PutMapping("/warranty/done/{id}")
	public ResponseEntity<?> updateWarrantyToDone(@PathVariable("id") long id)
	{
		return ResponseEntity.ok(centerService.updateWarrantyToDone(id));
	}
}
