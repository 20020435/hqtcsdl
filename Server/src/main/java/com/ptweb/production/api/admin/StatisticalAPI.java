package com.ptweb.production.api.admin;

import com.ptweb.production.payload.request.AdminStatisticalRequest;
import com.ptweb.production.service.IAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */
@CrossOrigin
@RestController(value = "statisticalOfAdmin")
@RequestMapping("/api/admin/statistical")
@Secured("ROLE_ADMIN")
public class StatisticalAPI
{
	@Autowired
	IAdminService adminService;

	@PostMapping("/status")
	public ResponseEntity<?> statisticalProductByStatus(@RequestBody AdminStatisticalRequest request)
	{
		return ResponseEntity.ok(adminService.statisticalProductStatus(request));
	}

	@PostMapping("/factory")
	public ResponseEntity<?> statisticalProductByFactory(@RequestBody AdminStatisticalRequest request)
	{
		return ResponseEntity.ok(adminService.statisticalProductFactory(request));
	}

	@PostMapping("/distribution-agent")
	public ResponseEntity<?> statisticalProductByDistribution(@RequestBody AdminStatisticalRequest request)
	{
		return ResponseEntity.ok(adminService.statisticalProductTransaction(request));
	}

	@PostMapping("/service-center")
	public ResponseEntity<?> statisticalProductByServiceCenter(@RequestBody AdminStatisticalRequest request)
	{
		return ResponseEntity.ok(adminService.statisticalProductWarranty(request));
	}
}
