package com.ptweb.production.api.factory;

import com.ptweb.production.payload.request.ExportProductRequest;
import com.ptweb.production.payload.response.MessageResponse;
import com.ptweb.production.payload.response.StatisticalResponse;
import com.ptweb.production.service.IFactoryService;
import com.ptweb.production.util.LoggerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@CrossOrigin
@RestController
@RequestMapping("/api/factory")
@Secured("ROLE_FACTORY")
public class FactoryAPI
{
	@Autowired
	IFactoryService factoryService;

	@PostMapping("/export")
	public ResponseEntity<?> exportProduct(@RequestBody ExportProductRequest request)
	{
		try
		{
			return ResponseEntity.ok(factoryService.exportProduct(request));
		}
		catch (Exception e)
		{
			LoggerUtils.log(e);
			return ResponseEntity.badRequest().body(new MessageResponse("Failed to export product!!!"));
		}
	}

	@GetMapping("/product")
	public ResponseEntity<?> getCountProduct()
	{
		try
		{
			return ResponseEntity.ok(factoryService.getCountProduct());
		}
		catch (Exception e)
		{
			LoggerUtils.log(e);
			return ResponseEntity.badRequest().body(new MessageResponse("Failed to get product!!!"));
		}
	}

	@GetMapping("/history/production")
	public ResponseEntity<?> getProductionHistory()
	{
		return ResponseEntity.ok(factoryService.getProductionHistory());
	}

	@GetMapping("/history/distribution")
	public ResponseEntity<?> getDistributionHistory()
	{
		return ResponseEntity.ok(factoryService.getDistributionHistory());
	}

	@GetMapping("/error-product")
	public ResponseEntity<?> getErrorProduct()
	{
		return ResponseEntity.ok(factoryService.getErrorProduct());
	}

	@PutMapping("/error-product/{id}")
	public ResponseEntity<?> updateErrorProduct(@PathVariable("id") long id)
	{
		return ResponseEntity.ok(factoryService.updateErrorProduct(id));
	}
}