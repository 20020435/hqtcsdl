package com.ptweb.production.api.serviceCenter;

import com.ptweb.production.payload.request.FactoryStatisticalRequest;
import com.ptweb.production.payload.request.ServiceCenterStatisticalRequest;
import com.ptweb.production.service.IFactoryService;
import com.ptweb.production.service.IServiceCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */
@CrossOrigin
@RestController(value = "statisticalOfServiceCenter")
@RequestMapping("/api/service-center/statistical")
@Secured("ROLE_SERVICE_CENTER")
public class StatisticalAPI
{
	@Autowired
	IServiceCenterService serviceCenterService;

	@PostMapping("/product")
	public ResponseEntity<?> statisticalProductByStatus(@RequestBody ServiceCenterStatisticalRequest request)
	{
		return ResponseEntity.ok(serviceCenterService.statisticalProduct(request));
	}
}
