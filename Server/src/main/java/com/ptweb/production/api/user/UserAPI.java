package com.ptweb.production.api.user;

import com.ptweb.production.entity.User;
import com.ptweb.production.payload.request.AuthRequest;
import com.ptweb.production.payload.request.BranchRequest;
import com.ptweb.production.payload.request.CategoryRequest;
import com.ptweb.production.payload.request.UserRequest;
import com.ptweb.production.payload.response.MessageResponse;
import com.ptweb.production.payload.response.UserResponse;
import com.ptweb.production.service.ICategoryService;
import com.ptweb.production.service.IUserService;
import com.ptweb.production.service.impl.UserDetailsImpl;
import com.ptweb.production.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api/user")
public class UserAPI
{
	@Autowired
	IUserService userService;

	@Secured("ROLE_ADMIN")
	@PostMapping
	public ResponseEntity<?> registerUser(@RequestBody UserRequest request)
	{
		UserResponse entity = userService.save(request);
		if(entity == null)
			return ResponseEntity.ok(new MessageResponse("Failed!! User Name already exist"));
		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}

	@PutMapping
	public ResponseEntity<?> updateUser(@RequestBody UserRequest request)
	{
		return ResponseEntity.ok(userService.update(request));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> registerUser(@PathVariable("id") long id)
	{
		userService.delete(id);
		return ResponseEntity.ok("ok");
	}

	@GetMapping("/userinfo")
	public ResponseEntity<?> getUserInfo()
	{
		UserDetailsImpl userDetails = SecurityUtils.getPrincipal();
		return ResponseEntity.ok(userService.findOneById(userDetails.getId()));
	}

	@Secured("ROLE_ADMIN")
	@GetMapping("/{id}")
	public ResponseEntity<?> getUserInfo(@PathVariable(name = "id") Long id)
	{
		return ResponseEntity.ok(userService.findOneById(id));
	}
}