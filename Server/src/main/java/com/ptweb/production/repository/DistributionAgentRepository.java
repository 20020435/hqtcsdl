package com.ptweb.production.repository;

import com.ptweb.production.entity.DistributionAgent;
import com.ptweb.production.entity.Product;
import com.ptweb.production.payload.response.CountProductResponse;
import com.ptweb.production.payload.response.ListWarrantyResponse;
import com.ptweb.production.payload.response.ProductWarehouseResponse;
import com.ptweb.production.payload.response.ProductWarrantyResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface DistributionAgentRepository extends JpaRepository<DistributionAgent, Long>
{
	@Query(value = "select id from distribution_agent where branch_id = ?1 LIMIT 1", nativeQuery = true)
	long getDistributionAgentIdByBranchId(long branchId);

	@Query("select new com.ptweb.production.payload.response.ProductWarehouseResponse" +
			"(count(p), (select w from WareHouse w where w = p.wareHouse)) from Product p where " +
			"p.distributionAgentId = ?1 and p.category.id = ?2 " +
			"and p.productStatus = com.ptweb.production.entity.ProductStatus.AT_DISTRIBUTION " +
			"group by p.wareHouse")
	List<ProductWarehouseResponse> getProductInWarehouse(long agentId, long categoryId);

	@Query("select p from Product p where p.wareHouse.id = ?1 and p.category.id = ?2 " +
			"and p.productStatus = com.ptweb.production.entity.ProductStatus.AT_DISTRIBUTION")
	List<Product> findFirstByWarehouseId(long warehouseId, long categoryId, Pageable pageable);

	@Query("select new com.ptweb.production.payload.response.ProductWarrantyResponse" +
			"(w.id, (select p from Product p where p.id = w.productId), " +
			"(select p.category from Product p where p.id = w.productId) , w.errorMessage) " +
			"from WarrantyHistory w where w.distributionAgentId = ?1 and w.status = 3")
	List<ProductWarrantyResponse> getWarrantyError(Long agentId);

	@Query("select new com.ptweb.production.payload.response.ProductWarrantyResponse" +
			"(w.id, (select p from Product p where p.id = w.productId), " +
			"(select p.category from Product p where p.id = w.productId) , w.errorMessage) " +
			"from WarrantyHistory w where w.distributionAgentId = ?1 and w.status = 2")
	List<ProductWarrantyResponse> getWarrantyDone(Long agentId);

	@Query(value = "select category_id, count(*) from v_transaction_product " +
			"where distribution_agent_id = ?1 and UNIX_TIMESTAMP(createddate)*1000 > ?2 " +
			"and UNIX_TIMESTAMP(createddate)*1000 < ?3 " +
			"group by category_id", nativeQuery = true)
	List<List<Long>> statisticalSellProduct(long agentId, long dateFrom, long dateTo);

	@Query("select new com.ptweb.production.payload.response.ListWarrantyResponse" +
			"(w.id, (select p from Product p where p.id = w.productId), " +
			"(select p.category from Product p where p.id = w.productId) , w.errorMessage, w.status) " +
			"from WarrantyHistory w where w.distributionAgentId = ?1 and w.status < 2")
	List<ListWarrantyResponse> getAllWarranty(Long agentId);
}