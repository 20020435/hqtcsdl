package com.ptweb.production.repository;

import com.ptweb.production.entity.ServiceCenter;
import com.ptweb.production.payload.response.ProductWarrantyResponse;
import com.ptweb.production.payload.response.StatisticalResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface ServiceCenterRepository extends JpaRepository<ServiceCenter, Long>
{
	@Query(value = "select id from service_center where branch_id = ?1 LIMIT 1", nativeQuery = true)
	long getServiceCenterIdByBranchId(long branchId);

	@Query("select new com.ptweb.production.payload.response.ProductWarrantyResponse" +
			"(w.id, (select p from Product p where p.id = w.productId), " +
			"(select p.category from Product p where p.id = w.productId) , w.errorMessage) " +
			"from WarrantyHistory w where w.serviceCenterId = ?1 and w.status = 0")
	List<ProductWarrantyResponse> getAllOrderWarranty(Long centerId);

	@Query("select new com.ptweb.production.payload.response.ProductWarrantyResponse" +
			"(w.id, (select p from Product p where p.id = w.productId), " +
			"(select p.category from Product p where p.id = w.productId) , w.errorMessage) " +
			"from WarrantyHistory w where w.serviceCenterId = ?1 and w.status = 1")
	List<ProductWarrantyResponse> getAllWarranty(Long centerId);

	@Query("select new com.ptweb.production.payload.response.StatisticalResponse" +
			"('Bảo hành thành công!', count(w)) from WarrantyHistory w where (w.status = 2 or w.status = 5) " +
			"and w.serviceCenterId = ?1 and w.createdDate between ?2 and ?3")
	StatisticalResponse statisticalProductDone(long centerId, Date dateFrom, Date dateTo);

	@Query("select new com.ptweb.production.payload.response.StatisticalResponse" +
			"('Không bảo hành được!', count(w)) from WarrantyHistory w where (w.status = 3 or w.status = 4) " +
			"and w.serviceCenterId = ?1 and w.createdDate between ?2 and ?3")
	StatisticalResponse statisticalProductError(long centerId, Date dateFrom, Date dateTo);
}