package com.ptweb.production.repository;

import com.ptweb.production.entity.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BranchRepository extends JpaRepository<Branch, Long>
{
	Branch findOneById(Long id);
}