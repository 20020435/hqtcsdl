package com.ptweb.production.repository;

import com.ptweb.production.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long>
{
	User findOneByUserName(String name);

	User findOneById(Long id);

	@Query(value = "SELECT count(*) FROM user", nativeQuery = true)
	long countUser();

	List<User> findAllByBranchId(long brachId);
}