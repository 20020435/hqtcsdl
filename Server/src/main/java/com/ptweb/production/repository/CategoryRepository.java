package com.ptweb.production.repository;

import com.ptweb.production.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long>
{
	Category findOneById(Long id);

	Category findOneByCode(String code);
}