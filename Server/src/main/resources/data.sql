CREATE TABLE IF NOT EXISTS `role` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
CREATE TABLE IF NOT EXISTS `refreshtoken` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `createdby` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `createddate` datetime DEFAULT NULL,
    `modifiedby` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `modifieddate` datetime DEFAULT NULL,
    `expiry_date` datetime DEFAULT NULL,
    `token` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `UK_or156wbneyk8noo4jstv55ii3` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
CREATE TABLE IF NOT EXISTS `user` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `createdby` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `createddate` datetime DEFAULT NULL,
    `modifiedby` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `modifieddate` datetime DEFAULT NULL,
    `address` text COLLATE utf8_bin,
    `fullname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `image` text COLLATE utf8_bin,
    `password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `phonenumber` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `refresh_token_id` bigint NOT NULL,
    `role_id` bigint DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FKo3wsa4hvodpbgd6upolukg7nj` (`refresh_token_id`),
    KEY `FKn82ha3ccdebhokx3a8fgdqeyy` (`role_id`),
    CONSTRAINT `FKn82ha3ccdebhokx3a8fgdqeyy` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
    CONSTRAINT `FKo3wsa4hvodpbgd6upolukg7nj` FOREIGN KEY (`refresh_token_id`) REFERENCES `refreshtoken` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
CREATE TABLE IF NOT EXISTS `ware_house` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `modifiedby` varchar(255) DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
CREATE TABLE IF NOT EXISTS `category` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `modifiedby` varchar(255) DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `content` text,
  `cylinder_capacity` varchar(255) DEFAULT NULL,
  `engine_type` varchar(255) DEFAULT NULL,
  `fuel_pump_system` varchar(255) DEFAULT NULL,
  `gear` varchar(255) DEFAULT NULL,
  `image` text,
  `max_speed` varchar(255) DEFAULT NULL,
  `maximum_output` varchar(255) DEFAULT NULL,
  `maximum_torque` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` bigint DEFAULT NULL,
  `short_description` text,
  `tank_capacity` varchar(255) DEFAULT NULL,
  `warranty_period` bigint DEFAULT '24',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
CREATE TABLE IF NOT EXISTS `product` (
  `createddate` date NOT NULL,
  `id` bigint NOT NULL,
  `createdby` varchar(255) DEFAULT NULL,
  `modifiedby` varchar(255) DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `code` varchar(255) NOT NULL,
  `distribution_agent_id` bigint DEFAULT NULL,
  `factory_id` bigint DEFAULT NULL,
  `status` int DEFAULT NULL,
  `service_center_id` bigint DEFAULT NULL,
  `warranty_count` bigint DEFAULT '0',
  `warranty_period_date` datetime DEFAULT NULL,
  `category_id` bigint DEFAULT NULL,
  `ware_house_id` bigint DEFAULT NULL,
  PRIMARY KEY (`createddate`,`id`),
  UNIQUE KEY `UK_h3w5r1mx6d0e5c6um32dgyjej` (`code`),
  KEY `IDXh3w5r1mx6d0e5c6um32dgyjej` (`code`),
  KEY `IDXi0vry5cr5df8il8d0cxh39lnm` (`status`),
  KEY `IDXrlaghtegr0yx2c1q1s6nkqjlh` (`category_id`),
  KEY `IDXl17vef6ish69u0bskua5b66e8` (`ware_house_id`),
  KEY `IDX7xs56rpvvgm6jbw3yei14v6c9` (`factory_id`,`status`),
  KEY `IDX7f23m6lb7aqn4cn8aygrf57bp` (`category_id`,`status`),
  KEY `IDXti7l9nynjxfvmjc8hq88vkx18` (`ware_house_id`,`category_id`,`status`),
  KEY `IDXtl6psadtd1mg1mym0lx6a9922` (`distribution_agent_id`,`category_id`,`status`),
  CONSTRAINT `FK10n3sc4rce95qvlwsnfa4kbe6` FOREIGN KEY (`ware_house_id`) REFERENCES `ware_house` (`id`),
  CONSTRAINT `FK1mtsbur82frn64de7balymq9s` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
CREATE TABLE IF NOT EXISTS `transaction_history` (
  `createddate` date NOT NULL,
  `id` bigint NOT NULL,
  `createdby` varchar(255) DEFAULT NULL,
  `modifiedby` varchar(255) DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `customer_age` bigint DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `distribution_agent_id` bigint DEFAULT NULL,
  `product_id` bigint DEFAULT NULL,
  PRIMARY KEY (`createddate`,`id`),
  KEY `IDXmbw5d0g80rngta7ooss5f6e7m` (`product_id`),
  KEY `IDXd5cxm0abwye8yvalvmfjwmolk` (`distribution_agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
INSERT INTO role (id, code, name) SELECT * FROM (SELECT '1' as id, 'ADMIN' as code, 'ADMIN' as name) AS tmp WHERE NOT EXISTS (SELECT code FROM role WHERE code = 'ADMIN') LIMIT 1;
INSERT INTO role (id, code, name) SELECT * FROM (SELECT '2' as id, 'DISTRIBUTION_AGENT' as code, 'DISTRIBUTION AGENT' as name) AS tmp WHERE NOT EXISTS (SELECT code FROM role WHERE code = 'DISTRIBUTION_AGENT') LIMIT 1;
INSERT INTO role (id, code, name) SELECT * FROM (SELECT '3' as id, 'FACTORY' as code, 'FACTORY' as name) AS tmp WHERE NOT EXISTS (SELECT code FROM role WHERE code = 'FACTORY') LIMIT 1;
INSERT INTO role (id, code, name) SELECT * FROM (SELECT '4' as id, 'SERVICE_CENTER' as code, 'SERVICE CENTER' as name) AS tmp WHERE NOT EXISTS (SELECT code FROM role WHERE code = 'SERVICE_CENTER') LIMIT 1;
INSERT INTO refreshtoken (id) SELECT * FROM (SELECT '1' as id) AS tmp WHERE NOT EXISTS (SELECT id FROM refreshtoken WHERE id = '1') LIMIT 1;
Delete from user where id = '1';
INSERT INTO `user` (`id`, `fullname`, `password`, `username`, `refresh_token_id`, `role_id`) VALUES ('1', 'ADMIN', '$2a$10$fXPNGnniIlAtoBpGQGZBbuCScdj4Pdnj.BLd2lujHr0cYo2z7epzy', 'admin', '1', '1');

create or replace view v_transaction_product as select t.*, p.code, p.category_id from transaction_history t, product p where t.product_id = p.id;