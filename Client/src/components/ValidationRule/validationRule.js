import { phoneNumberRegex, strongPasswordRegex } from "./auth-regex";

export const emptyValidation = {
    validator: async (_, value) => {
        if(!value) return Promise.reject(
            new Error("Không được bỏ trống thông tin!")
        );
        if(value.trim() !== '') {
            return Promise.resolve();
        };
        return Promise.reject(
            new Error('Thông tin không được là khoảng trắng!')
        );
    },
};

export const phoneNumberValidation = {
    validator: async ( _, value = "" ) => {
      const phoneNumber = value.trim();
      const checkIsValid = phoneNumberRegex.test(phoneNumber);
      if (!checkIsValid && phoneNumber) return Promise.reject(
        new Error("Số điện thoại không hợp lệ!")
      );
      return Promise.resolve();
    }
  };
  
  export const strongPasswordValidation = {
    validator: async ( _, value ) => {
      if(value){
        const password = value.trim();
        const checkIsValid = strongPasswordRegex.test(password);
        if (!checkIsValid && password) return Promise.reject(
          new Error("Mật khẩu tối thiểu 8 chữ số!")
        );
        return Promise.resolve();
      }
    }
  };