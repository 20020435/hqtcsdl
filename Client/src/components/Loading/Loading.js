import { useSelector } from "react-redux";
import classNames from "classnames/bind";
import style from "./Loading.module.scss";

const cx = classNames.bind(style);

export default function Loading() {
  const isLoading = useSelector((state) => state.loading.value);

  return (
    isLoading && (
      <div className={cx("loading")}>
        <div className={cx("loading-progess")}></div>
      </div>
    )
  );
}
