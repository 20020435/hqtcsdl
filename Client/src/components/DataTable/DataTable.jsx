import classNames from "classnames/bind";
import style from "./DataTable.module.scss";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faList } from "@fortawesome/free-solid-svg-icons";
import { useEffect, useState } from "react";
import Button from "../Button";

const cx = classNames.bind(style);
library.add(faList);

const DataTable = ({ data, dataItems }) => {
  const [minWidth, setMinWidth] = useState({});

  useEffect(() => {
    const widths = {};
    dataItems.forEach((item) => {
      if (item.actions) widths[item.value] = 100 * item.actions.length;
      else {
        const heading = document.getElementById(`heading-item-${item.value}`);
        let maxValue = 0;
        data.forEach((el, i) => {
          const value = document.getElementById(`data-item-${item.value}-${i}`);
          if (value.offsetWidth > maxValue) maxValue = value.offsetWidth;
        });
        if (heading && maxValue)
          widths[item.value] =
            heading.offsetWidth > maxValue ? heading.offsetWidth : maxValue;
      }
      if (widths[item.value]) widths[item.value] += 20;
    });
    setMinWidth(widths);
  }, [data]);

  return (
    <div className={cx("table")}>
      {!data || data.length === 0 ? (
        <div className={cx("no-data")}>
          <FontAwesomeIcon icon="fa-solid fa-list" />
          <span>no data</span>
        </div>
      ) : (
        <>
          <div className={cx("table-heading")}>
            {dataItems.map((item, i) => (
              <p
                key={i}
                className={cx("heading-item")}
                style={{
                  flex: item.flex,
                  justifyContent: `flex-${item.align}`,
                }}
              >
                <span
                  style={{ width: minWidth[item.value] }}
                  id={`heading-item-${item.value}`}
                >
                  {item.heading}
                </span>
              </p>
            ))}
          </div>
          <div className={cx("table-data")}>
            {data.map((row, idx) => (
              <li key={idx} className={cx("data-row")}>
                {dataItems.map((item, i) => (
                  <p
                    key={i}
                    className={cx("data-item")}
                    style={{
                      flex: item.flex,
                      justifyContent: `flex-${item.align}`,
                    }}
                  >
                    <span
                      className=""
                      style={{ width: minWidth[item.value] || "max-content" }}
                      id={`data-item-${item.value}-${idx}`}
                    >
                      {item.actions
                        ? item.actions.map((action, j) => (
                            <span style={{ marginRight: "10px" }} key={j}>
                              <Button
                                text={action.text}
                                color={action.color || "#5d7b6f"}
                                onclick={() => action.click(row[action.bind])}
                                outlined
                              />
                            </span>
                          ))
                        : item.format
                        ? item.format(row[item.value])
                        : row[item.value]}
                    </span>
                  </p>
                ))}
              </li>
            ))}
          </div>
        </>
      )}
    </div>
  );
};

export default DataTable;
