import classNames from "classnames/bind";
import { useLayoutEffect } from "react";
import { useSelector } from "react-redux";
import style from "./Button.module.scss";

const cx = classNames.bind(style);

const btnColor = {
  red: "red",
};

export default function Button({
  text,
  onclick,
  textColor,
  color,
  outlined,
  upCase,
  fontSize,
}) {
  const $isLoading = useSelector((state) => state.loading.value);

  useLayoutEffect(() => {
    if (upCase) text = text.toUpperCase();
  });

  const style = {
    fontSize: fontSize ? fontSize + "px" : "16px",
    background: btnColor[color] || color || "black",
    color: btnColor[textColor] || textColor || "black",
    border: "0",
  };

  const outlinedStyle = {
    fontSize: fontSize ? fontSize + "px" : "16px",
    background: "transparent",
    border: `1px solid ${btnColor[color] || color || "black"}`,
    color:
      btnColor[textColor] || textColor || btnColor[color] || color || "black",
  };

  return (
    <button
      className={cx("button")}
      disabled={$isLoading}
      onClick={onclick}
      style={outlined ? outlinedStyle : style}
    >
      {text}
    </button>
  );
}
