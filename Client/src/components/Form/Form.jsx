import { forwardRef, useImperativeHandle, useRef } from "react";
import FormItem from "./FormItem";
import ValidateMessage from "./ValidateMessage";
import classNames from "classnames/bind";
import style from "./Form.module.scss";

const cx = classNames.bind(style);

const Form = forwardRef(({ form, items, rlt }, ref) => {
  const validator = useRef({});
  const formItemRef = useRef([]);

  useImperativeHandle(ref, () => ({
    submit(onsubmit) {
      if (validate(items)) {
        onsubmit();
        formItemRef.current.forEach((e) => {
          e.clear();
        });
      }
    },
  }));

  const validate = (items) => {
    let isValid = true;
    for (let item of items) {
      let errorMessage = "";
      if (item.rules)
        for (let rule of item.rules) {
          errorMessage = rule(form[item.name]);
          validator.current[item.name].clearMessage();
          if (errorMessage) {
            validator.current[item.name].getMessage(errorMessage);
            isValid = false;
            break;
          }
        }
    }
    return isValid;
  };

  const handleItem = (item) => {
    form[item.name] = item.value;
  };

  return (
    <div className={cx("form")}>
      {form && (
        <div
          className={cx("form-input")}
          style={{
            justifyContent: rlt ? "flex-end" : "flex-start",
          }}
        >
          {items.map((item, index) => (
            <div
              className={cx("form-item")}
              key={index}
              style={{
                width: `${8.33333 * (item.col || 1)}%`,
              }}
            >
              {
                <FormItem
                  ref={(e) => (formItemRef.current[index] = e)}
                  item={item}
                  onchange={(inputValue) => {
                    handleItem({ name: [item.name], value: inputValue });
                    if (item.onchange) item.onchange();
                  }}
                  onblur={() => validate([item])}
                />
              }
              <ValidateMessage
                ref={(element) => {
                  validator.current[item.name] = element;
                }}
              />
            </div>
          ))}
        </div>
      )}
    </div>
  );
});

export default Form;
