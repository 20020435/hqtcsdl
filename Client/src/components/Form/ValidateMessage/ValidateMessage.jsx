import { forwardRef, useImperativeHandle, useState } from "react";
import classNames from "classnames/bind";
import style from "./ValidateMessage.module.scss";

const cx = classNames.bind(style);

const ValidateMessage = forwardRef((props, ref) => {
  const [message, setMessage] = useState("");
  useImperativeHandle(ref, () => ({
    getMessage(newMessage) {
      setMessage(newMessage);
    },
    clearMessage() {
      setMessage("");
    },
  }));
  return (
    <div key={message} className={cx("validate")}>
      {message}
    </div>
  );
});

export default ValidateMessage;
