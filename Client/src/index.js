import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import GlobalStyles from "@/components/layout-components/GlobalStyles";
import { persistor, store } from "./store";
import { BrowserRouter } from "react-router-dom";
import Loading from "./components/Loading";
import Toast from "./components/Toast";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render (
  <React.StrictMode>
    <GlobalStyles>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <BrowserRouter>
            <App />
          </BrowserRouter>
          <Toast />
          <Loading />
        </PersistGate>
      </Provider>
    </GlobalStyles>
  </React.StrictMode>
);

<App />;
