import { ManufacturerLayout } from "@/layouts";
import { Route } from "react-router-dom";
import NoPage from "../NoPage";
import Export from "./Export";
import DashBoard from "./DashBoard";
import Insurance from "./Insurance";
import Product from "./Products";

const ManufacturerPages = () => {
  return (
    <>
      <Route
        exact
        index
        element={
          <ManufacturerLayout pageTitle="DashBoard">
            <DashBoard />
          </ManufacturerLayout>
        }
      />
      <Route
        exact
        path="products"
        element={
          <ManufacturerLayout pageTitle="Products">
            <Product />
          </ManufacturerLayout>
        }
      />
      <Route
        exact
        path="export"
        element={
          <ManufacturerLayout pageTitle="Export">
            <Export />
          </ManufacturerLayout>
        }
      />
      <Route
        exact
        path="insurance"
        element={
          <ManufacturerLayout pageTitle="Insurance">
            <Insurance />
          </ManufacturerLayout>
        }
      />
      <Route
        path="*"
        element={
          <ManufacturerLayout pageTitle="Test">
            <NoPage />
          </ManufacturerLayout>
        }
      />
    </>
  );
};

export default ManufacturerPages;
