import Card from "@/components/Card";
import DataChart from "@/components/DataChart";
import Form from "@/components/Form";
import { useApi } from "@/hooks/useApi";
import classNames from "classnames/bind";
import { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import style from "./DashBoard.module.scss";

const cx = classNames.bind(style);

const statuses = [];

export default function DashBoard() {
  const [dataBugRate, setDataBugRate] = useState([]);
  const [dataProductExported, setDataProductExported] = useState([]);
  const [dataProductByStatus, setDataProductByStatus] = useState([]);
  const productStatuses = useSelector((state) => state.data.productStatuses);
  const $api = useApi();

  const getProductsExported = async () => {
    const raw = await $api.getProductsExported();
    const data = raw.map((item) => ({
      name: item.category.name,
      value: item.count,
    }));
    setDataProductExported(data);
  };

  const getProductByStatus = async (statuses) => {
    const raw = await $api.getProductByStatus(statuses);
    const data = raw.map((item) => ({
      name: item.category.name,
      value: item.count,
    }));
    setDataProductByStatus(data);
  };

  const getBugRate = async (statuses) => {
    const raw = await $api.getBugRate(statuses);
    const data = raw.map((item) => ({
      name: item.category.name,
      value: item.rate,
    }));
    setDataBugRate(data);
  };

  useEffect(() => {
    getProductsExported();
    getProductByStatus();
    getBugRate();
  }, []);

  const [statusForm] = useState({
    ...productStatuses.map((item) => ({ [item.id]: false })),
    dateFrom: "",
    dateTo: "",
  });

  const [exportForm] = useState({
    dateFrom: "",
    dateTo: "",
  });

  const [bugForm] = useState({
    dateFrom: "",
    dateTo: "",
  });

  const statusFormItems = [
    ...productStatuses.map((item) => ({
      type: "checkbox",
      name: item.id,
      title: item.name,
      rlt: true,
      col: 2,
      onchange: () => {
        if (statusForm[item.id] !== "error") {
          if (statusForm[item.id]) statuses.push(item.id);
          else {
            const index = statuses.indexOf(item.id);
            if (index > -1) {
              statuses.splice(index, 1);
            }
          }
          const form = { statuses: [...statuses] };
          if (statusForm.dateFrom) form.dateFrom = statusForm.dateFrom;
          if (statusForm.dateTo) form.dateTo = statusForm.dateTo;
          if (statusForm.dateTo !== "Invalid Date") getProductByStatus(form);
        }
      },
    })),
    {
      type: "spacer",
      col: 12,
    },
    {
      type: "pickDate",
      name: "dateFrom",
      title: "From",
      col: 3,
      rules: [(v) => (v !== "Invalid Date" ? "" : "Ngày không hợp lệ")],
      onchange: () => {
        const form = { statuses: [...statuses] };
        if (statusForm.dateFrom) form.dateFrom = statusForm.dateFrom;
        if (statusForm.dateTo) form.dateTo = statusForm.dateTo;
        if (statusForm.dateTo !== "Invalid Date") getProductByStatus(form);
      },
    },
    {
      type: "pickDate",
      name: "dateTo",
      title: "To",
      col: 3,
      rules: [(v) => (v !== "Invalid Date" ? "" : "Ngày không hợp lệ")],
      onchange: () => {
        const form = { statuses: [...statuses] };
        if (statusForm.dateFrom) form.dateFrom = statusForm.dateFrom;
        if (statusForm.dateTo) form.dateTo = statusForm.dateTo;
        if (statusForm.dateTo !== "Invalid Date") getProductByStatus(form);
      },
    },
  ];

  const exportItems = [
    {
      type: "pickDate",
      name: "dateFrom",
      title: "From",
      col: 4,
      rules: [(v) => (v !== "Invalid Date" ? "" : "Ngày không hợp lệ")],
      onchange: () => {
        const form = {};
        if (exportForm.dateFrom) form.dateFrom = exportForm.dateFrom;
        if (exportForm.dateTo) form.dateTo = exportForm.dateTo;
        if (exportForm.dateTo !== "Invalid Date") getProductsExported(form);
      },
    },
    {
      type: "pickDate",
      name: "dateTo",
      title: "To",
      col: 4,
      rules: [(v) => (v !== "Invalid Date" ? "" : "Ngày không hợp lệ")],
      onchange: () => {
        const form = {};
        if (exportForm.dateFrom) form.dateFrom = exportForm.dateFrom;
        if (exportForm.dateTo) form.dateTo = exportForm.dateTo;
        if (exportForm.dateTo !== "Invalid Date") getProductsExported(form);
      },
    },
  ];

  const bugItems = [
    {
      type: "pickDate",
      name: "dateFrom",
      title: "From",
      col: 4,
      rules: [(v) => (v !== "Invalid Date" ? "" : "Ngày không hợp lệ")],
      onchange: () => {
        const form = {};
        if (bugForm.dateFrom) form.dateFrom = bugForm.dateFrom;
        if (bugForm.dateTo) form.dateTo = bugForm.dateTo;
        if (bugForm.dateTo !== "Invalid Date") getBugRate(form);
      },
    },
    {
      type: "pickDate",
      name: "dateTo",
      title: "To",
      col: 4,
      rules: [(v) => (v !== "Invalid Date" ? "" : "Ngày không hợp lệ")],
      onchange: () => {
        const form = {};
        if (bugForm.dateFrom) form.dateFrom = bugForm.dateFrom;
        if (bugForm.dateTo) form.dateTo = bugForm.dateTo;
        if (bugForm.dateTo !== "Invalid Date") getBugRate(form);
      },
    },
  ];

  return (
    <div className={cx("dashboard")}>
      <Card width="100%">
        <div className={cx("heading")}>
          <h3 style={{ width: "max-content" }}>Thống kê sản phẩm</h3>
          <div style={{ flex: 1 }}>
            <Form form={statusForm} items={statusFormItems} rlt />
          </div>
        </div>
        <DataChart
          height={600}
          data={dataProductByStatus}
          maxValue={50}
          nameField="Thể loại"
          valueField="Số lượng"
        />
      </Card>
      <Card width="50%">
        <div className={cx("heading")}>
          <h3>Thống kê xuất hàng</h3>
          <div style={{ flex: 1 }}>
            <Form form={exportForm} items={exportItems} rlt />
          </div>
        </div>
        <DataChart
          height={500}
          data={dataProductExported}
          maxValue={10}
          nameField="Thể loại"
          valueField="Số lượng"
        />
      </Card>
      <Card width="50%">
        <div className={cx("heading")}>
          <h3>Thống kê lỗi</h3>
          <div style={{ flex: 1 }}>
            <Form form={bugForm} items={bugItems} rlt />
          </div>
        </div>
        <DataChart
          height={500}
          data={dataBugRate}
          maxValue={100}
          nameField="Thể loại"
          valueField="Tỉ lệ lỗi"
          unit="%"
        />
      </Card>
    </div>
  );
}
