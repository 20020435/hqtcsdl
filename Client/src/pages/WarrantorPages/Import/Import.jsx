import Card from "@/components/Card";
import { useApi } from "@/hooks/useApi";
import { useLayoutEffect, useState } from "react";
import classNames from "classnames/bind";
import style from "./Import.module.scss";
import { useLoading } from "@/hooks/useLoading";
import DataTable from "@/components/DataTable";
import { useToast } from "@/hooks/useToast";

const cx = classNames.bind(style);

export default function Import() {
  const [data, setData] = useState([]);
  const $loading = useLoading();
  const $api = useApi();
  const $toast = useToast();

  const getWarrantyOrders = async () => {
    const raw = await $api.getWarrantyOrders();
    const data = [
      ...raw.map((item) => ({ ...item, category: item.category.name })),
    ];
    setData(data);
  };

  const acceptWarrantyOrder = async (id) => {
    $loading.start();
    let success = await $api.acceptWarrantyOrder(id);
    if (success) {
      getWarrantyOrders();
      $toast.show({ type: "success", message: "Duyệt thành công!" });
    } else
      $toast.show({
        type: "error",
        message: "Có lỗi xảy ra, không thể duyệt!",
      });
    $loading.stop();
  };

  useLayoutEffect(() => {
    getWarrantyOrders();
  }, []);

  const dataItems = [
    {
      value: "productId",
      heading: "ID Sản phẩm",
      flex: 3,
      align: "start",
    },
    {
      value: "code",
      heading: "Mã Sản phẩm",
      flex: 1,
      align: "start",
    },
    {
      value: "category",
      heading: "Thể loại",
      flex: 3,
      align: "start",
    },
    {
      value: "errorMessage",
      heading: "Ghi chú",
      flex: 1,
      align: "end",
    },
    {
      value: "actions",
      heading: "Actions",
      actions: [
        {
          text: "Duyệt",
          bind: "id",
          color: "#00b300",
          click: (bind) => {
            acceptWarrantyOrder(bind);
          },
        },
      ],
      flex: 3,
      align: "end",
    },
  ];

  return (
    <div className={cx("export")}>
      <Card width="100%">
        <h3>Danh sách sản phẩm lỗi đang chờ</h3>
        <DataTable data={data} dataItems={dataItems} />
      </Card>
    </div>
  );
}
