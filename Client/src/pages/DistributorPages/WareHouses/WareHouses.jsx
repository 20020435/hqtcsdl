import { useState, useRef, useLayoutEffect, useEffect } from "react";
import { SearchOutlined } from "@ant-design/icons";
import { Button, Input, Space, Table, Modal } from "antd";
import Highlighter from "react-highlight-words";
import { useApi } from "@/hooks/useApi";
import classNames from "classnames/bind";
import style from "./WareHouses.module.scss";
import { Link } from "react-router-dom";
import { useToast } from "@/hooks/useToast";

const cx = classNames.bind(style);

const WareHouses = () => {
  const $api = useApi();
  const $toast = useToast();
  const [warehouses, setWarehouses] = useState([]);
  const [isOpenConfirmModal, setIsOpenConfirmModal] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [modalText, setModalText] = useState("Bạn chắc chắn muốn xóa nhà kho này?");
  const [warehouseId, setWarehouseId] = useState("");

  const getWarehouses = async () => {
    const data = await $api.getAllWarehouses();
    setWarehouses(data);
  };

  useEffect(() => {
    getWarehouses();
  }, []);

  const handleWarehouse = (id)  => {
    showModal();
    setWarehouseId(id);
  }
  const showModal = () => {
    setIsOpenConfirmModal(true);
  };

  const handleOK = async () => {
    setModalText("Đang xác nhận...");
    let success = await $api.deleteWarehouse(warehouseId);
    if (success) {
      getWarehouses();    
      setIsOpenConfirmModal(false);
      setConfirmLoading(false);
      setModalText("Bạn chắc chắn muốn xoá nhà kho này?");
      $toast.show({
        type: "success",
        message: `Xóa thành công nhà kho có id = ${warehouseId}!`,
      });
    } else {
      $toast.show({
        type: "error",
        message: "Có lỗi xảy ra!",
      });
      setModalText('Xóa kho thất bại!')
    }
  }

  const handleCancel = () => {
    setIsOpenConfirmModal(false);
  };


  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const searchInput = useRef(null);
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };
  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };
  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
      close,
    }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: "block",
          }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{
              width: 100,
            }}
          >
            Tìm kiếm
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{
              width: 90,
            }}
          >
            Làm mới
          </Button>

          <Button
            type="link"
            size="small"
            onClick={() => {
              close();
            }}
          >
            Đóng
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? "#1890ff" : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: "#ffc069",
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });
  const columns = [
    {
      title: "Id",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Tên nhà kho",
      dataIndex: "name",
      key: "name",
      ...getColumnSearchProps("name"),
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      key: "address",
      ...getColumnSearchProps("address"),
    },

    {
      title: "Hành động",
      dataIndex: "",
      key: "",
      render: (item) => (
        <div
          className={cx("table__delete-btn")}
          onClick={() => handleWarehouse(item.id)}
        >
          Xóa
        </div>
      ),
    },
  ];

  return (
    <div className={cx("distributor-wrap")}>
      <h1 className={cx("distributor__heading")}>Danh sách các nhà kho</h1>
      <div>
        <Link to={"/distribution-agent/warehouse/new"}>
          <button className={cx("distributor__btn", "distributor__btn--right")}>
            Thêm nhà kho
          </button>
        </Link>

        <Table
          className={cx("distributor__table")}
          columns={columns}
          dataSource={warehouses}
          pagination={{ pageSize: 5 }}
          rowKey="id"
        ></Table>
        <Modal
          title="Xoá nhà kho"
          open={isOpenConfirmModal}
          onOk={handleOK}
          cancelText={"Hủy"}
          okText={"Xác nhận"}
          confirmLoading={confirmLoading}
          onCancel={handleCancel}
          centered={true}
        >
          <p className={cx("modal-text")}>{modalText}</p>
        </Modal>
      </div>
    </div>
  );
};

export default WareHouses;
