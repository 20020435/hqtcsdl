import { useState, useRef, useLayoutEffect, useEffect } from "react";
import { SearchOutlined } from "@ant-design/icons";
import { Button, Input, Space, Table, Modal, Cascader } from "antd";
import Highlighter from "react-highlight-words";
import { useApi } from "@/hooks/useApi";
import classNames from "classnames/bind";
import style from "./Orders.module.scss";
import { Link } from "react-router-dom";
import Utils from "@/utils";
import Form from "@/components/Form";
import { useToast } from "@/hooks/useToast";

const cx = classNames.bind(style);

const Orders = () => {
  const $api = useApi();
  const $toast = useToast();
  const [orders, setOrders] = useState([]);
  const [isOpenConfirmModal, setIsOpenConfirmModal] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [modalText, setModalText] = useState("Bạn chắc chắn muốn xác nhận?");
  const [orderId, setOrderId] = useState("");
  const formElement = useRef(null);

  const getAllOrders = async () => {
    const data = await $api.getAllOrders();
    setOrders(data);
  };
  const getWarehouses = async () => {
    await $api.getAllWarehouses();
  };

  useEffect(() => {
    getAllOrders();
    getWarehouses();
  }, []);

  const handleConfirm = (orderId) => {
    console.log(orderId);
    showModal();
    setOrderId(orderId);
  };

  const showModal = () => {
    setIsOpenConfirmModal(true);
  };

  const handleOK = async () => {
    setModalText("Đang xác nhận...");
    let success = await $api.confirmOrder(orderId, form.warehouseId);
    if (form.warehouseId!== "" && success) {
      getAllOrders();
      setIsOpenConfirmModal(false);
      setConfirmLoading(false);
      setModalText("Bạn chắc chắn muốn xác nhận?");
      $toast.show({
        type: "success",
        message: `Đưa thành công vào nhà kho số ${form.warehouseId}!`,
      });
    } else {
      $toast.show({
        type: "error",
        message: "Có lỗi xảy ra!",
      });
      setModalText('Xác nhận thất bại!')
    }
  };

  const handleCancel = () => {
    setIsOpenConfirmModal(false);
  };

  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const searchInput = useRef(null);
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };
  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };
  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
      close,
    }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: "block",
          }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{
              width: 100,
            }}
          >
            Tìm kiếm
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{
              width: 90,
            }}
          >
            Làm mới
          </Button>

          <Button
            type="link"
            size="small"
            onClick={() => {
              close();
            }}
          >
            Đóng
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? "#1890ff" : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: "#ffc069",
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const columns = [
    {
      title: "Id",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Tên cơ sở sản xuất",
      dataIndex: "factory",
      key: "factory",
      ...getColumnSearchProps("factory"),
    },
    {
      title: "Ảnh sản phẩm",
      dataIndex: "",
      key: "",
      render: (item) => (
        <img
          src={item.category.image}
          alt="Ảnh sản phẩm"
          style={{ width: "80px", height: "50px" }}
        />
      ),
    },
    {
      title: "Tên sản phẩm",
      dataIndex: "",
      key: "",
      render: (item) => <div>{item.category.name}</div>,
    },
    {
      title: "Thông tin chi tiết",
      dataIndex: "",
      key: "",
      render: (item) => <div>{item.category.content}</div>,
    },
    {
      title: "Giá 1 sản phẩm",
      dataIndex: "",
      key: "",
      render: (item) => (
        <div>{`${Utils.convertPriceValueType(item.category.price)} VNĐ`}</div>
      ),
    },
    {
      title: "Số lượng",
      dataIndex: "count",
      key: "count",
    },

    {
      title: "Hành động",
      dataIndex: "",
      key: "",
      render: (item) => (
        <div
          className={cx("table__confirm-btn")}
          onClick={() => handleConfirm(item.id)}
        >
          Xác nhận
        </div>
      ),
    },
  ];

  const [form, setForm] = useState({
    warehouseId: "",
  });

  const formItems = [
    {
      type: "dropList",
      name: "warehouseId",
      options: "warehouses",
      textOptions: "name",
      valueOptions: "id",
      rules: [
        (v) => (v ? "" : "Vui lòng nhập nhà kho"),
        (v) => (v !== "error" ? "" : "nhà kho không hợp lệ"),
      ],
      col: 12,
    },
  ];

  return (
    <div className={cx("distributor-wrap")}>
      <h1 className={cx("distributor__heading")}>Danh sách các đơn hàng nhập từ CSSX</h1>
      <Table
        className={cx("distributor__table", "orders__table")}
        columns={columns}
        dataSource={orders}
        pagination={{ pageSize: 5 }}
        rowKey = 'id'
      ></Table>

      <Modal
        title="Chọn nhà kho"
        open={isOpenConfirmModal}
        onOk={handleOK}
        cancelText={"Hủy"}
        okText={"Xác nhận"}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        centered={true}
      >
        <Form ref={formElement} form={form} items={formItems} />
        <p className={cx('modal-text')}>{modalText}</p>
      </Modal>
    </div>
  );
};

export default Orders;
