import Form from "@/components/Form";
import { useApi } from "@/hooks/useApi";
import { Column } from "@ant-design/plots";
import { useEffect, useState } from "react";
import classNames from "classnames/bind";
import style from "./Home.module.scss";

const cx = classNames.bind(style);

export default function Home() {
  const [dataBugRate, setDataBugRate] = useState([]);
  const [bugForm] = useState({
    dateFrom: "",
    dateTo: "",
  });
  const config = {
    data: dataBugRate,
    xField: "name",
    yField: "value",
    xAxis: {
      label: {
        autoHide: true,
        autoRotate: false,
      },
    },
    scrollbar: {
      type: "horizontal",
    },
    minColumnWidth: 30,
    maxColumnWidth: 30,
  };
  const $api = useApi();

  const bugItems = [
    {
      type: "pickDate",
      name: "dateFrom",
      title: "From",
      col: 4,
      rules: [(v) => (v !== "Invalid Date" ? "" : "Ngày không hợp lệ")],
      onchange: () => {
        const form = {};
        if (bugForm.dateFrom) form.dateFrom = bugForm.dateFrom;
        if (bugForm.dateTo) form.dateTo = bugForm.dateTo;
        if (bugForm.dateTo !== "Invalid Date") getNumberOfProducts(form);
      },
    },
    {
      type: "pickDate",
      name: "dateTo",
      title: "To",
      col: 4,
      rules: [(v) => (v !== "Invalid Date" ? "" : "Ngày không hợp lệ")],
      onchange: () => {
        const form = {};
        if (bugForm.dateFrom) form.dateFrom = bugForm.dateFrom;
        if (bugForm.dateTo) form.dateTo = bugForm.dateTo;
        if (bugForm.dateTo !== "Invalid Date") getNumberOfProducts(form);
      },
    },
  ];

  const getNumberOfProducts = async (statuses) => {
    const raw = await $api.getNumberOfProducts(statuses);
    const data = raw.map((item) => ({
      name: item.category.name,
      value: item.count,
    }));
    setDataBugRate(data);
  };

  useEffect(() => {
    getNumberOfProducts();
  }, []);
  return (
    <div className={cx("distributor-wrap")}>
      <h1 className={cx("distributor__heading")}>Danh sách các đơn hàng</h1>
      <div className={cx("home__table")}>
        <Form form={bugForm} items={bugItems} />

        <Column {...config} />
      </div>
    </div>
  );
}
