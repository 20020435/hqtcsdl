import { useRef } from "react";
import { useToast } from "@/hooks/useToast";
import Form from "@/components/Form";
import { useLoading } from "@/hooks/useLoading";
import classNames from "classnames/bind";
import style from "./Login.module.scss";
import Card from "@/components/Card";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLock, faUser } from "@fortawesome/free-solid-svg-icons";
import { useApi } from "@/hooks/useApi";
import { useLogin } from "@/hooks/useLogin";
import { useSelector } from "react-redux";

const cx = classNames.bind(style);
library.add(faUser, faLock);

const Login = () => {
  const $isLoading = useSelector((state) => state.loading.value);
  const formElement = useRef(null);
  const form = useRef({
    username: "",
    password: "",
  });
  const $toast = useToast();
  const $loading = useLoading();
  const $api = useApi();
  const $login = useLogin();

  const handleSubmit = async () => {
    $loading.start();
    const res = await $api.getAccessToken(
      { ...form.current },
      form.current.remember
    );
    if (res) {
      $toast.show({ type: "success", message: "Đăng nhập thành công!" });
      $login.return();
    } else
      $toast.show({ type: "error", message: "Đăng nhập không thành công!" });
    $loading.stop();
  };

  const formItems = [
    {
      type: "textField",
      name: "username",
      title: "User name",
      icon: <FontAwesomeIcon icon="fa-solid fa-user" />,
      rules: [(v) => (v ? "" : "Vui lòng nhập Username")],
      col: 12,
    },
    {
      type: "password",
      name: "password",
      title: "Password",
      icon: <FontAwesomeIcon icon="fa-solid fa-lock" />,
      rules: [(v) => (v ? "" : "Vui lòng nhập Password")],
      col: 12,
    },
    {
      type: "checkbox",
      name: "remember",
      title: "Remember me",
      col: 3,
    },
    {
      type: "spacer",
      col: 7,
    },
  ];

  return (
    <div className={cx("login")}>
      <Card width="700px" justify="center">
        <h1>Login</h1>
        <Form ref={formElement} form={form.current} items={formItems} />
        <button
          className={cx("submit")}
          disabled={$isLoading}
          onClick={() => formElement.current.submit(handleSubmit)}
        >
          Đăng nhập
        </button>
      </Card>
    </div>
  );
};

export default Login;
