import { useState, useRef, useLayoutEffect, useEffect } from "react";
import { SearchOutlined } from "@ant-design/icons";
import { Button, Input, Space, Table, Modal } from "antd";
import Highlighter from "react-highlight-words";
import { useApi } from "@/hooks/useApi";
import classNames from "classnames/bind";
import style from "./ProductsManagement.scss";
import { Link, useNavigate } from "react-router-dom";
import Utils from "@/utils";
import { useToast } from "@/hooks/useToast";

const cx = classNames.bind(style);

const ProductsManagement = () => {
  const $navigate = useNavigate();
  const $api = useApi();
  const $toast = useToast();
  const [products, setProducts] = useState([]);
  const [isOpenConfirmModal, setIsOpenConfirmModal] = useState(false);
  const [isOpenDetails, setIsOpenDetails] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [modalText, setModalText] = useState(
    "Bạn chắc chắn muốn xóa sản phẩm này?"
  );
  const [cateforyId, setCategoryId] = useState("");

  const getProducts = async () => {
    const data = await $api.getAllCategory();
    setProducts(data);
  };

  useEffect(() => {
    getProducts();
  }, []);

  const handleDeleteProduct = (id) => {
    showModal();
    setCategoryId(id);
  };
  const showModal = () => {
    setIsOpenConfirmModal(true);
  };

  const handleOK = async () => {
    setModalText("Đang xác nhận...");
    let success = await $api.deleteCategory(cateforyId);
    if (success) {
      getProducts();
      setIsOpenConfirmModal(false);
      setConfirmLoading(false);
      setModalText("Bạn chắc chắn muốn xoá sản phẩm này?");
      $toast.show({
        type: "success",
        message: `Xóa thành công sản phẩm có id = ${cateforyId}!`,
      });
    } else {
      $toast.show({
        type: "error",
        message: "Có lỗi xảy ra!",
      });
      setModalText("Xóa sản phẩm thất bại!");
    }
  };

  const handleCancel = () => {
    setIsOpenConfirmModal(false);
  };

  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const searchInput = useRef(null);
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };
  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };

  const handleEditProduct = (item) => {
    $navigate(`edit/${item.id}`, { state: item });
  };

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
      close,
    }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: "block",
          }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{
              width: 100,
            }}
          >
            Tìm kiếm
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{
              width: 90,
            }}
          >
            Làm mới
          </Button>

          <Button
            type="link"
            size="small"
            onClick={() => {
              close();
            }}
          >
            Đóng
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? "#1890ff" : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: "#ffc069",
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });
  const columns = [
    {
      title: "Id",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Ảnh sản phẩm",
      dataIndex: "image",
      key: "image",
      render: (image) => (
        <img src={image} alt="" style={{ width: "80px", height: "50px" }} />
      ),
    },
    {
      title: "Tên sản phẩm",
      dataIndex: "name",
      key: "name",
      ...getColumnSearchProps("name"),
    },
    {
      title: "Mã sản phẩm",
      dataIndex: "code",
      key: "code",
      ...getColumnSearchProps("code"),
    },
    {
      title: "Mô tả",
      dataIndex: "content",
      key: "content",
      width: "30%",
    },
    {
      title: "Thông tin cấu hình",
      dataIndex: "",
      key: "",
      width: "25%",
      render: (item) => (
        <ul>
          <li className={cx("list-item")}>
            <strong>Kiểu động cơ: </strong>
            {item.engineType}
          </li>
          <li className={cx("list-item")}>
            <strong>Hộp số: </strong>
            {item.gear}
          </li>
          <li className={cx("list-item")}>
            <strong>Dung tích xi lanh (cm³): </strong>
            {item.cylinderCapacity}
          </li>
          <li className={cx("list-item")}>
            <strong>Công suất cực đại (Hp/rpm): </strong>
            {item.maximumTorque}
          </li>
          <li className={cx("list-item")}>
            <strong>Tốc độ tối đa (km/h): </strong>
            {item.maxSpeed}
          </li>
          <li className={cx("list-item")}>
            <strong>Dung tích thùng nhiên liệu (lít): </strong>
            {item.tankCapacity}
          </li>
          <li className={cx("list-item")}>
            <strong>Hệ thống bơm nhiên liệu: </strong>
            {item.fuelPumpSystem}
          </li>
        </ul>
      ),
    },
    {
      title: "Giá bán",
      dataIndex: "price",
      key: "price",
      ...getColumnSearchProps("price"),
      render: (price) => (
        <div>{`${Utils.convertPriceValueType(price)} VNĐ`}</div>
      ),
    },
    {
      title: "Hành động",
      dataIndex: "",
      key: "x",
      render: (item) => {
        return (
          <div style={{ display: "flex" }}>
            <div
              className={cx("table__accept-btn")}
              onClick={() => handleEditProduct(item)}
            >
              Sửa
            </div>
            <div
              style={{ marginLeft: 20 }}
              className={cx("table__delete-btn")}
              onClick={() => handleDeleteProduct(item.id)}
            >
              Xóa
            </div>
          </div>
        );
      },
    },
  ];

  return (
    <div className={cx("manager-wrap")}>
      <h1 className={cx("manager__heading")}>Danh sách dòng sản phẩm</h1>
      <div>
        <Link to={"/admin/products/new"}>
          <button className={cx("manager__btn", "manager__btn--right")}>
            Thêm sản phẩm
          </button>
        </Link>
        <Table
          className={cx("manager__table")}
          columns={columns}
          dataSource={products}
          pagination={{ pageSize: 5 }}
          rowKey="id"
        ></Table>
        <Modal
          title="Xoá sản phẩm"
          open={isOpenConfirmModal}
          onOk={handleOK}
          cancelText={"Hủy"}
          okText={"Xác nhận"}
          confirmLoading={confirmLoading}
          onCancel={handleCancel}
          centered={true}
        >
          <p className={cx("modal-text")}>{modalText}</p>
        </Modal>
        <Modal
          title="Xoá sản phẩm"
          open={isOpenDetails}
          onOk={() => setIsOpenDetails}
          okText={"Xác nhận"}
          centered={true}
        >
          <p className={cx("modal-text")}>{modalText}</p>
        </Modal>
      </div>
    </div>
  );
};

export default ProductsManagement;
