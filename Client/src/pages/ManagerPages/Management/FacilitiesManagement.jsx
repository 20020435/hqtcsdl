import { useState, useRef, useLayoutEffect, useEffect } from "react";
import { SearchOutlined } from "@ant-design/icons";
import { Button, Input, Space, Table, Modal } from "antd";
import Highlighter from "react-highlight-words";
import { useApi } from "@/hooks/useApi";
import classNames from "classnames/bind";
import style from "./FacilitiesManagement.module.scss";
import { Link } from "react-router-dom";
import { useToast } from "@/hooks/useToast";

const cx = classNames.bind(style);

const FacilitiesManagement = () => {
  const $api = useApi();
  const [factilities, setFacilities] = useState([]);
  const $toast = useToast();
  const [isOpenConfirmModal, setIsOpenConfirmModal] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [modalText, setModalText] = useState("Bạn chắc chắn muốn xóa cơ sở này?");
  const [facilityId, setFacilityId] = useState("");

  const getFacilities = async () => {
    const data = await $api.getAllFacilities();
    setFacilities(data);
  };

  useEffect(() => {
    getFacilities();
  }, []);

  const handleDeleteFacility = (id) => {
    showModal();
    setFacilityId(id);
  };
  const showModal = () => {
    setIsOpenConfirmModal(true);
  };

  const handleOK = async () => {
    setModalText("Đang xác nhận...");
    let success = await $api.deleteFacility(facilityId);
    if (success) {
      getFacilities();    
      setIsOpenConfirmModal(false);
      setConfirmLoading(false);
      setModalText("Bạn chắc chắn muốn xoá cơ sở này?");
      $toast.show({
        type: "success",
        message: `Xóa thành công cơ sở có id = ${facilityId}!`,
      });
    } else {
      $toast.show({
        type: "error",
        message: "Có lỗi xảy ra!",
      });
      setModalText('Xóa cơ sở thất bại!')
    }
  }

  const handleCancel = () => {
    setIsOpenConfirmModal(false);
  };


  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const searchInput = useRef(null);
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };
  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };
  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
      close,
    }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: "block",
          }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{
              width: 100,
            }}
          >
            Tìm kiếm
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{
              width: 90,
            }}
          >
            Làm mới
          </Button>

          <Button
            type="link"
            size="small"
            onClick={() => {
              close();
            }}
          >
            Đóng
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? "#1890ff" : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: "#ffc069",
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });
  const columns = [
    {
      title: "Id",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Tên cơ sở",
      dataIndex: "name",
      key: "name",
      ...getColumnSearchProps("name"),
    },
    {
      title: "Loại cơ sở",
      dataIndex: "",
      key: "",
      render: (item) => {
        let typeFacility = "";
        switch (item.role) {
          case "distribution-agent":
            typeFacility = "Đại lý phân phối";
            break;
          case "factory":
            typeFacility = "Cơ sở sản xuất";
            break;
          case "service-center":
            typeFacility = "Trung tâm bảo hành";
            break;
          default:
            typeFacility = "";
        }

        return <div>{typeFacility}</div>
      },
    },
    {
      title: "Địa chỉ cơ sở",
      dataIndex: "address",
      key: "address",
      ...getColumnSearchProps("address"),
    },
    {
      title: "Hành động",
      dataIndex: "",
      key: "",
      render: (item) => (
        <div>
          <div
            className={cx("table__delete-btn")}
            onClick={() => handleDeleteFacility(item.id)}
          >
            Xóa
          </div>
          <Link to={`/admin/facilities-management/${item.id}`}>
            <div className={cx("table__detail-btn")}>Chi tiết</div>
          </Link>
        </div>
      ),
    },
  ];

  return (
    <div className={cx("manager-wrap")}>
      <h1 className={cx("manager__heading")}>Danh sách các cơ sở</h1>
      <div>
        <Link to={"/admin/facilities-management/new"}>
          <button className={cx("manager__btn", "manager__btn--right")}>
            Thêm cơ sở
          </button>
        </Link>
        <Table
          className={cx("manager__table")}
          columns={columns}
          dataSource={factilities}
          pagination={{ pageSize: 5 }}
          rowKey="id"
        ></Table>

        <Modal
          title="Xoá sản phẩm"
          open={isOpenConfirmModal}
          onOk={handleOK}
          cancelText={"Hủy"}
          okText={"Xác nhận"}
          confirmLoading={confirmLoading}
          onCancel={handleCancel}
          centered={true}
        >
          <p className={cx("modal-text")}>{modalText}</p>
        </Modal>
      </div>
    </div>
  );
};

export default FacilitiesManagement;
