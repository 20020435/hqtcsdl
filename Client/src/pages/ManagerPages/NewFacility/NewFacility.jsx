import React from "react";
import classNames from "classnames/bind";
import style from "./NewFacility.module.scss";
import { Form, Input, message, Select, Cascader } from "antd";
import { emptyValidation } from "@/components/ValidationRule/validationRule";
import { Link } from "react-router-dom";
import { useApi } from "@/hooks/useApi";
import { useToast } from "@/hooks/useToast";
import { useLoading } from "@/hooks/useLoading";

const cx = classNames.bind(style);
const NewFacility = () => {
  const $api = useApi();
  const $toast = useToast();
  const $loading = useLoading();
  const options = [
    {
      value: "distribution-agent",
      label: "Đại lí phân phối",
    },
    {
      value: "factory",
      label: "Cơ sở sản xuất",
    },
    {
      value: "service-center",
      label: "Trung tâm bảo hành",
    },
  ];

  const handleCreateFacility = async () => {
    $loading.start();
    const name = document.querySelector('input[name = "facilityName"]').value;
    const address = document.querySelector(
      'input[name = "facilityAddress"]'
    ).value;
    const category =
      document.getElementById("facility__cascader")?.parentElement
        ?.nextElementSibling?.innerHTML;
    const form = document.querySelector('form[id = "facility-form"]');
    if (
      category &&
      category !== "Vui lòng chọn thể loại" &&
      name !== "" &&
      form
    ) {
      const body = {
        name,
        address,
        role: category,
      };
      let success = await $api.createNewFacility(body);
      if (success) {
        $toast.show({
          type: "success",
          message: `Thêm thành công cơ sở ${name}!`,
        });
        form.reset();
      } else {
        $toast.show({
          type: "error",
          message: "Có lỗi xảy ra khi thêm cơ sở!",
        });
      }
    }
    $loading.stop();
  };

  return (
    <div className={cx("manager-wrap")}>
      <h1 className={cx("manager__heading")}>Thêm cơ sở</h1>
      <Link to={"/admin/facilities-management"}>
        <button className={cx("manager__btn", "manager__btn--left")}>
          Quay lại
        </button>
      </Link>

      <div className={cx("manager-form")}>
        <Form
          id="facility-form"
          name="basic"
          layout="horizontal"
          initialValues={{ remember: true }}
          autoComplete="off"
        >
          <Form.Item
            label={<p className={cx("manager-form-label")}>Tên cơ sở</p>}
            name="facilityName"
            rules={[{ required: true, message: "Vui lòng nhập tên cơ sở!" }]}
          >
            <Input
              className={cx("manager-form-input")}
              placeholder="Nhập tên cơ sở"
              name="facilityName"
            />
          </Form.Item>
          <Form.Item
            label={<p className={cx("manager-form-label")}>Địa chỉ</p>}
            name="facilityAddress"
            rules={[{ required: true, message: "Vui lòng nhập địa chỉ!" }]}
          >
            <Input
              className={cx("manager-form-input")}
              type="text"
              placeholder="Nhập tên sản phẩm muốn thêm"
              name="facilityAddress"
            />
          </Form.Item>
          <Form.Item
            label={
              <p
                className={cx("manager-form-label", "manager__label-category")}
              >
                Loại cơ sở
              </p>
            }
            name="facilityCategory"
          >
            <Input.Group>
              <Cascader
                id="facility__cascader"
                style={{ width: "100%", textAlign: "left" }}
                options={options}
                placeholder="Vui lòng chọn thể loại"
                name="facilityCategory"
              />
            </Input.Group>
          </Form.Item>

          <Form.Item>
            <button
              className={cx("manager-submit-btn")}
              type="submit"
              onClick={handleCreateFacility}
            >
              Thêm cơ sở
            </button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default NewFacility;
