import classNames from "classnames/bind";
import style from "./Statistic.module.scss";
import { Form, Input, Cascader, Select } from "antd";
import React, { useState, useEffect, memo } from "react";
import { Column } from "@ant-design/plots";
import { useApi } from "@/hooks/useApi";
import Form2 from "@/components/Form";

const cx = classNames.bind(style);

const Statistic = () => {
  const [dateForm, setDateForm] = useState({});
  const [allCategory, setAllCategory] = useState([]);
  const [categories, setCategories] = useState([]);
  const [products, setProducts] = useState([]);
  const [type, setType] = useState("status");
  const [config] = useState({
    data: products,
    xField: "name",
    yField: "count",
    xAxis: {
      label: {
        autoHide: true,
        autoRotate: false,
      },
    },
    scrollbar: {
      type: "horizontal",
    },
    minColumnWidth: 30,
    maxColumnWidth: 30,
  });

  const $api = useApi();

  useEffect(() => {
    getAllCategory();
  }, []);

  const getAllCategory = async () => {
    const data = await $api.getAllCategory();
    setCategories(data.map((item) => item.id));
    getProductQuantity(
      type,
      data.map((item) => item.id),
      dateForm
    );
    setAllCategory(data);
  };

  const getProductQuantity = async (type, categories, date) => {
    const form = {};
    form.ids = [...categories];
    form.dateFrom = date.dateFrom;
    form.dateTo = date.dateTo;
    const data = await $api.getProductQuantity(type, { ...form });
    config.data = [...data];
    setProducts(data);
  };

  const handleChangCategory = (value) => {
    getProductQuantity(type, [...value.map((item) => item[1])], dateForm);
    setCategories([...value.map((item) => item[1])]);
  };

  const handleTypeChange = (value) => {
    if (categories.length === 0)
      getProductQuantity(
        value,
        allCategory.map((item) => item.id),
        dateForm
      );
    else getProductQuantity(value, categories, dateForm);
    setType(value);
  };

  const dateFormItems = [
    {
      type: "pickDate",
      name: "dateFrom",
      title: "Ngày bắt đầu",
      col: 5,
      rules: [(v) => (v !== "Invalid Date" ? "" : "Ngày không hợp lệ")],
      onchange: () => {
        const form = {};
        if (dateForm.dateFrom) form.dateFrom = dateForm.dateFrom;
        if (dateForm.dateTo) form.dateTo = dateForm.dateTo;
        if (dateForm.dateTo !== "Invalid Date")
          getProductQuantity(type, categories, form);
      },
    },
    {
      type: "spacer",
      col: 2,
    },
    {
      type: "pickDate",
      name: "dateTo",
      title: "Ngày kết thúc",
      col: 5,
      rules: [(v) => (v !== "Invalid Date" ? "" : "Ngày không hợp lệ")],
      onchange: () => {
        const form = {};
        if (dateForm.dateFrom) form.dateFrom = dateForm.dateFrom;
        if (dateForm.dateTo) form.dateTo = dateForm.dateTo;
        if (dateForm.dateTo !== "Invalid Date")
          getProductQuantity(type, categories, form);
      },
    },
  ];

  const typeOptions = [
    {
      value: "status",
      label: "Theo trạng thái",
    },
    {
      value: "factory",
      label: "Theo cơ sở sản xuất",
    },
    {
      value: "distribution-agent",
      label: "Theo đại lý phân phối",
    },
    {
      value: "service-center",
      label: "Theo trung tâm bảo hành",
    },
  ];

  const { SHOW_CHILD } = Cascader;

  const categoryOptions = [
    {
      label: "Tất cả sản phẩm",
      children: allCategory.map((item) => ({
        label: `${item.name}`,
        value: `${item.id}`,
      })),
    },
  ];

  return (
    <div className={cx("statistic-wrap")}>
      <h1 className={cx("manager__heading", "statistic__heading")}>
        Thống kê số lượng sản phẩm
      </h1>
      <div style={{ display: "flex" }}>
        <Form style={{ flex: 1, alignItems: "center" }}>
          <Form.Item
            label={<p className={cx("manager-form-label")}>Thống kê</p>}
            name="statisticType"
            style={{ width: "50%", marginTop: 20, display: "inline-block" }}
            className="cx"
          >
            <Input.Group>
              <Select
                onChange={handleTypeChange}
                defaultValue={"Theo trạng thái"}
                style={{ width: 220 }}
                options={typeOptions}
              />
            </Input.Group>
          </Form.Item>

          <Form.Item
            label={<p className={cx("manager-form-label")}>Thể loại</p>}
            name="statisticCategory"
            style={{
              width: "45%",
              marginLeft: 20,
              marginTop: 20,
              display: "inline-block",
              position: "relative",
            }}
          >
            <Input.Group>
              <Cascader
                style={{ width: 250 }}
                options={categoryOptions}
                multiple
                maxTagCount="responsive"
                showCheckedStrategy={SHOW_CHILD}
                onChange={handleChangCategory}
              />
            </Input.Group>
          </Form.Item>
        </Form>
        <div style={{ flex: 1 }}>
          <Form2 form={dateForm} items={dateFormItems} />
        </div>
      </div>

      <Column {...config} />
    </div>
  );
};

export default memo(Statistic);
