import { ManagerLayout } from "@/layouts";
import { Route, Link, useParams } from "react-router-dom";
import ProductsManagement from "./Products";
import FacilitiesManagement from "./Management";
import NewProduct from "./NewProduct";
import NewFacility from "./NewFacility";
import Statistic from "./Statistic";
import DetailFacility from "./DetailFacility";
import NewUserAccount from "./NewUserAccount";

const ManagerPages = () => {
  return (
    <>
      <Route
        exact
        index
        element={
          <ManagerLayout>
            <Statistic />
          </ManagerLayout>
        }
      />
      <Route
        exact
        path="admin/products"
        element={
          <ManagerLayout>
            <ProductsManagement />
          </ManagerLayout>
        }
      />
      <Route
        exact
        path="admin/products/new"
        element={
          <ManagerLayout>
            <NewProduct />
          </ManagerLayout>
        }
      />
      <Route
        exact
        path="admin/products/edit/:id"
        element={
          <ManagerLayout>
            <NewProduct />
          </ManagerLayout>
        }
      />
      <Route
        exact
        path="admin/facilities-management"
        element={
          <ManagerLayout>
            <FacilitiesManagement />
          </ManagerLayout>
        }
      />
      <Route
        exact
        path="admin/facilities-management/new"
        element={
          <ManagerLayout>
            <NewFacility />
          </ManagerLayout>
        }
      />
      <Route
        exact
        path="admin/facilities-management/:id"
        element={
          <ManagerLayout>
            <DetailFacility />
          </ManagerLayout>
        }
      />
      <Route
        exact
        path="admin/facilities-management/:id/new"
        element={
          <ManagerLayout>
            <NewUserAccount />
          </ManagerLayout>
        }
      />
    </>
  );
};

export default ManagerPages;
