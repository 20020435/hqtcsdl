import React from "react";
import classNames from "classnames/bind";
import style from "./NewUserAccount.module.scss";
import { Form, Input, message, Select, Cascader } from "antd";
import { emptyValidation } from "@/components/ValidationRule/validationRule";
import { Link, useParams } from "react-router-dom";
import { useApi } from "@/hooks/useApi";
import { useToast } from "@/hooks/useToast";
import { useLoading } from "@/hooks/useLoading";
import { phoneNumberValidation, strongPasswordValidation } from "@/components/ValidationRule/validationRule";

const cx = classNames.bind(style);

const NewUserAccount = () => {
  let { id } = useParams();

  const $api = useApi();
  const $toast = useToast();
  const $loading = useLoading();
  const options = [
    {
      value: "distribution-agent",
      label: "distribution-agent",
    },
    {
      value: "factory",
      label: "factory",
    },
    {
      value: "service-center",
      label: "service-center",
    },
  ];

  const handleCreateUserAccount = async () => {
    $loading.start();
    const fullName = document.querySelector(
      'input[name = "accountFullName"]'
    ).value;
    const username = document.querySelector(
      'input[name = "accountUserName"]'
    ).value;
    const phoneNumber = document.querySelector(
      'input[name = "accountPhoneNumber"]'
    ).value;
    const password = document.querySelector(
        'input[name = "accountPassword"]'
      ).value;

    const form = document.querySelector('form[id = "account-form"]');
    console.log(id);

    if (form) {
      const body = {
        fullName,
        username,
        phoneNumber,
        password,
        branchId: id,
      };
      let success = await $api.createNewUserAccount(body);
      if (success) {
        $toast.show({
          type: "success",
          message: `Thêm thành công tài khoản ${username}!`,
        });
        form.reset();
      } else {
        $toast.show({
          type: "error",
          message: "Có lỗi xảy ra khi thêm tài khoản!",
        });
      }
    }
    $loading.stop()
  };

  return (
    <div className={cx("manager-wrap")}>
      <h1 className={cx("manager__heading")}>Thêm tài khoản các cơ sở</h1>
      <Link to={`/admin/facilities-management/${id}`}>
        <button className={cx("manager__btn", "manager__btn--left")}>
          Quay lại
        </button>
      </Link>

      <div className={cx("manager-form")}>
        <Form
          id="account-form"
          name="basic"
          layout="horizontal"
          initialValues={{ remember: true }}
          autoComplete="off"
        >
          <Form.Item
            label={<p className={cx("manager-form-label")}>Họ tên</p>}
            name="accountFullName"
            rules={[
              { required: true, message: "Vui lòng nhập tên tài khoản!" },
            ]}
          >
            <Input
              className={cx("manager-form-input")}
              placeholder="Nhập họ tên người dùng"
              name="accountFullName"
            />
          </Form.Item>
          <Form.Item
            label={<p className={cx("manager-form-label")}>Tên tài khoản</p>}
            name="accountUserName"
            rules={[
              { required: true, message: "Vui lòng nhập tên tài khoản!" },
            ]}
          >
            <Input
              className={cx("manager-form-input")}
              placeholder="Nhập tên tài khoản"
              name="accountUserName"
            />
          </Form.Item>
          <Form.Item
            label={<p className={cx("manager-form-label")}>Số điện thoại</p>}
            name="accountPhoneNumber"
            rules={[
              { required: true, message: "Vui lòng nhập số điện thoại!" },
              phoneNumberValidation
            ]}
          >
            <Input
              className={cx("manager-form-input")}
              placeholder="Nhập số điện thoại"
              name="accountPhoneNumber"
            />
          </Form.Item>

          <Form.Item
            label={<p className={cx("manager-form-label")}>Mật khẩu</p>}
            name="accountPassword"
            rules={[{ required: true, message: "Vui lòng nhập mật khẩu!" }, strongPasswordValidation]}
          >
            <Input.Password
              className={cx("manager-form-input")}
              placeholder="Nhập mật khẩu"
              name="accountPassword"
            />
          </Form.Item>
          
          <Form.Item>
            <button
              className={cx("manager-submit-btn")}
              type="submit"
              onClick={handleCreateUserAccount}
            >
              Thêm tài khoản
            </button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default NewUserAccount;
