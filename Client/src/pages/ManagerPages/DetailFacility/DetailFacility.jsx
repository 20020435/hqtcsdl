import { useParams, useSearchParams } from "react-router-dom";
import { useApi } from "@/hooks/useApi";
import { useState, useEffect, useRef } from "react";
import { Button, Input, Space, Table, Modal } from "antd";
import Highlighter from "react-highlight-words";
import classNames from "classnames/bind";
import style from "./DetailFacility.module.scss";
import { Link } from "react-router-dom";
import { SearchOutlined } from "@ant-design/icons";
import { useToast } from "@/hooks/useToast";

const cx = classNames.bind(style);

const DetailFacility = () => {
  let { id } = useParams();
  const $api = useApi();
  const [facility, setFacility] = useState([]);
  const [users, setUsers] = useState([]);
  const [products, setProducts] = useState([]);
  const [isOpenConfirmModal, setIsOpenConfirmModal] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [modalText, setModalText] = useState(
    "Bạn chắc chắn muốn xóa tài khoản này?"
  );
  const [accountId, setAccountId] = useState("");

  const getDetailOfFacility = async () => {
    const data = await $api.getDetailOfFacility(id);
    setFacility([data]);
  };

  const getAllUsers = async () => {
    const data = await $api.getAllUsersFacility(id);
    setUsers(data);
  };

  useEffect(() => {
    getAllUsers();
    getDetailOfFacility();
  }, []);

  const handleDeleteAccount = (id) => {
    showModal();
    setAccountId(id);
  };
  const showModal = () => {
    setIsOpenConfirmModal(true);
  };

  const handleOK = async () => {
    setModalText("Đang xác nhận...");
    let success = await $api.deleteAccount(accountId);
    if (success) {
      getAllUsers();
      setIsOpenConfirmModal(false);
      setConfirmLoading(false);
      setModalText("Bạn chắc chắn muốn xoá tài khoản này?");
      $toast.show({
        type: "success",
        message: `Xóa thành công tài khoản có id = ${accountId}!`,
      });
    } else {
      $toast.show({
        type: "error",
        message: "Có lỗi xảy ra!",
      });
      setModalText("Xóa tài khoản thất bại!");
    }
  };
  const handleCancel = () => {
    setIsOpenConfirmModal(false);
  };

  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const searchInput = useRef(null);
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };
  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };
  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
      close,
    }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: "block",
          }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{
              width: 100,
            }}
          >
            Tìm kiếm
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{
              width: 90,
            }}
          >
            Làm mới
          </Button>

          <Button
            type="link"
            size="small"
            onClick={() => {
              close();
            }}
          >
            Đóng
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? "#1890ff" : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: "#ffc069",
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const detailFacilityColumns = [
    {
      title: "Tên cơ sở",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Loại cơ sở",
      dataIndex: "",
      key: "",
      render: (item) => {
        let typeFacility = "";
        switch (item.role) {
          case "distribution-agent":
            typeFacility = "Đại lý phân phối";
            break;
          case "factory":
            typeFacility = "Cơ sở sản xuất";
            break;
          case "service-center":
            typeFacility = "Trung tâm bảo hành";
            break;
          default:
            typeFacility = "";
        }

        return <div>{typeFacility}</div>
      },
    },
    {
      title: "Địa chỉ cơ sở",
      dataIndex: "address",
      key: "address",
    },
  ];

  const allUsersColumns = [
    {
      title: "Id",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Tên tài khoản",
      dataIndex: "username",
      key: "username",
      ...getColumnSearchProps("username"),
    },
    {
      title: "Họ tên",
      dataIndex: "fullName",
      key: "fullName",
      ...getColumnSearchProps("fullName"),
    },
    {
      title: "Số điện thoại",
      dataIndex: "phoneNumber",
      key: "phoneNumber",
      ...getColumnSearchProps("phoneNumber"),
    },
    {
      title: "Cơ sở làm việc",
      dataIndex: "",
      key: "",
      render: (item) => {
        let typeFacility = "";
        switch (item.role) {
          case "DISTRIBUTION_AGENT":
            typeFacility = "Đại lý phân phối";
            break;
          case "FACTORY":
            typeFacility = "Cơ sở sản xuất";
            break;
          case "SERVICE_CENTER":
            typeFacility = "Trung tâm bảo hành";
            break;
          default:
            typeFacility = "";
        }

        return <div>{typeFacility}</div>
      },
    },
    {
      title: "Hành động",
      dataIndex: "",
      key: "",
      render: (item) => (
        <div
          className={cx("table__delete-btn")}
          onClick={() => handleDeleteAccount(item.id)}
        >
          Xóa
        </div>
      ),
    },
  ];

  return (
    <div className={cx("manager-wrap")}>
      <div>
        <h1 className={cx("manager__heading")}>
          Thông tin chi tiết cơ sở {id}
        </h1>
        <Table
          className={cx("detaiFacility__table")}
          dataSource={facility}
          columns={detailFacilityColumns}
          pagination={false}
        ></Table>
      </div>

      <div>
        <h1 className={cx("manager__heading", "detaiFacility__heading")}>
          Thông tin tài khoản của cơ sở
        </h1>
        <Link to={`/admin/facilities-management/${id}/new`}>
          <button className={cx("manager__btn", "manager__btn--right")}>
            Thêm tài khoản
          </button>
        </Link>
        <Table
          className={cx("manager__table")}
          dataSource={users}
          columns={allUsersColumns}
          pagination={{ pageSize: 5 }}
          rowKey="id"
        ></Table>
        <Modal
          title="Xoá tài khoản người dùng"
          open={isOpenConfirmModal}
          onOk={handleOK}
          cancelText={"Hủy"}
          okText={"Xác nhận"}
          confirmLoading={confirmLoading}
          onCancel={handleCancel}
          centered={true}
        >
          <p className={cx("modal-text")}>{modalText}</p>
        </Modal>
      </div>
    </div>
  );
};

export default DetailFacility;
