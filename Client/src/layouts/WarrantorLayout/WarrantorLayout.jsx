import classNames from "classnames/bind";
import styles from "./WarrantorLayout.module.scss";
import Menu from "@/components/layout-components/Menu";
import PageTitle from "@/components/PageTitle";

const cx = classNames.bind(styles);

const WarrantorLayout = ({ children, pageTitle }) => {
  return (
    <div className={cx("wrapper")}>
      <Menu />
      <div className={cx("content")}>
        {pageTitle && <PageTitle text={pageTitle} />}
        {children}
      </div>
    </div>
  );
};

export default WarrantorLayout;
