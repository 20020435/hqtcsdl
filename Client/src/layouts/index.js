export {default as DistributorLayout} from './DistributorLayout/DistributorLayout.jsx'
export {default as ManagerLayout} from './ManagerLayout/ManagerLayout.jsx'
export {default as ManufacturerLayout} from './ManufacturerLayout/ManufacturerLayout.jsx'
export {default as WarrantorLayout} from './WarrantorLayout/WarrantorLayout.jsx'