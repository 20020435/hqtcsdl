import classNames from "classnames/bind";
import styles from "./DistributorLayout.module.scss";
import Menu from "@/components/layout-components/Menu";

const cx = classNames.bind(styles);

const DistributorLayout = ({ children }) => {
  return (
    <div className={cx("wrapper")}>
      <Menu />

      <div className={cx("content")}>{children}</div>
    </div>
  );
};

export default DistributorLayout;
