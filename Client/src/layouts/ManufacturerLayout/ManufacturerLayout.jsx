import classNames from "classnames/bind";
import styles from './ManufacturerLayout.module.scss'
import Menu from '@/components/layout-components/Menu'
import PageTitle from "@/components/PageTitle";

const cx = classNames.bind(styles);;

const ManufacturerLayout = ({children, pageTitle}) => {
    return (
        <div className={cx("wrapper")}>
            <Menu />
            <div className={cx("content")}>
            {pageTitle && <PageTitle text={pageTitle} />}
                {children}
            </div>
        </div>
    )
}

export default ManufacturerLayout;