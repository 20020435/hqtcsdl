import { useDispatch, useSelector } from "react-redux";
import { storeHistoryPath, removeHistoryPath } from "@/store/Slices/loginSlice";
import { useNavigate } from "react-router-dom";
import { removeUser } from "@/store/Slices/userSlice";
import { removeLocalToken } from "@/service/token";

const useLogin = () => {
  const $dispatch = useDispatch();
  const historyPath = useSelector((state) => state.login.historyPath);
  const $navigate = useNavigate();
  return {
    subscribe: (path) => {
      if (path !== "/login" && path) $dispatch(storeHistoryPath(path));
      $navigate("login");
    },
    return: () => {
      $navigate(historyPath);
    },
    logOut: () => {
      $dispatch(removeHistoryPath());
      $dispatch(removeUser());
      removeLocalToken();
      $navigate("/login");
    },
  };
};

export { useLogin };
