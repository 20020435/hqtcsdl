import { createSlice } from "@reduxjs/toolkit";

const dataSlice = createSlice({
  name: "data",
  initialState: {
    categories: [],
    distributions: [],
    quantityCategory: [],
    productStatuses: [],
    warehouses: [],
    serviceCenters: [],
  },
  reducers: {
    setCategories: (state, { payload }) => {
      state.categories = [...payload];
    },
    setDistributions: (state, { payload }) => {
      state.distributions = [...payload];
    },
    setQuantityCategory: (state, { payload }) => {
      state.quantityCategory = [...payload];
    },
    setProductStatuses: (state, { payload }) => {
      state.productStatuses = [...payload];
    },
    setWarehouses: (state, {payload}) => {
      state.warehouses = [...payload];
    },
    setServiceCenters: (state, {payload}) => {
      state.serviceCenters = [...payload];
    }

  },
});

export const {
  setCategories,
  setDistributions,
  setQuantityCategory,
  setProductStatuses,
  setWarehouses,
  setServiceCenters,
} = dataSlice.actions;

export default dataSlice.reducer;
