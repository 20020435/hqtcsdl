import { combineReducers, configureStore } from "@reduxjs/toolkit";
import storage from "redux-persist/lib/storage";
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from "redux-persist";
import toastSlice from "./Slices/toastSlice";
import loadingSlice from "./Slices/loadingSlice";
import userSlice from "./Slices/userSlice";
import loginSlice from "./Slices/loginSlice";
import dataSlice from "./Slices/dataSlice";

const rootReducer = combineReducers({
  user: userSlice,
  loading: loadingSlice,
  toast: toastSlice,
  login: loginSlice,
  data: dataSlice,
});

const persistConfig = {
  key: "root",
  blacklist: ["loading", "toast", "data"],
  storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
});

export const persistor = persistStore(store);
