import { Routes, Route, Outlet } from "react-router-dom";
import Login from "@/pages/Login";
import NoPage from "@/pages/NoPage";
import { useEffect, useLayoutEffect } from "react";
import { getLocalAccessToken, getLocalRefreshToken } from "./service/token";
import { useSelector } from "react-redux";
import { useApi } from "@/hooks/useApi";
import { useLogin } from "./hooks/useLogin";
import ManagerPages from "./pages/ManagerPages";
import ManufacturerPages from "./pages/ManufacturerPages";
import WarrantorPages from "./pages/WarrantorPages";
import DistributorPages from "./pages/DistributorPages";

function App() {
  const accessToken = getLocalAccessToken();
  const localRefreshToken = getLocalRefreshToken();
  const role = useSelector((state) => state.user.role);
  const $api = useApi();
  const $login = useLogin();

  useLayoutEffect(() => {
    if (accessToken) {
      $api.getUserInfo();
    } else {
      console.log("didnthaveaccessToken");
      if (localRefreshToken) {
        console.log("buthaveRefreshToken");
        const renderToken = async () => {
          return await $api.getAccessToken({ refreshToken: localRefreshToken });
        };
        renderToken();
      }
    }
  }, [accessToken]);

  useEffect(() => {
    if (!accessToken && window.location.pathname !== "/login") {
      $login.subscribe({ ...{ ...window }.location }.pathname);
    }
  });

  return (
    <Routes>
      <Route path="/" element={<Outlet />}>
        {role === "ADMIN" && ManagerPages()}
        {role === "FACTORY" && ManufacturerPages()}
        {role === "SERVICE_CENTER" && WarrantorPages()}
        {role === "DISTRIBUTION_AGENT" && DistributorPages()}
        {!role && <Route element={<NoPage />} />}
      </Route>
      <Route exact path="login" element={<Login />} />
      <Route path="*" element={<NoPage />} />
    </Routes>
  );
}

export default App;
